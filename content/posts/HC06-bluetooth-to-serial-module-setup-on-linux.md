---
title: "HC06 Bluetooth to Serial Module Setup on Linux"
date: 2019-05-01T18:04:13+03:00
draft: false
tags: [ "linux" ]
---

HC06 module is a bluetooth serial adapter you can connect to a serial port (to microcontroller, raspberry pi...). This allows you to easily inspect serial messages directly on your android device or laptop.


![](https://raw.githubusercontent.com/adrianalin/NodeMCU_workspace/master/pictures/hc06.jpg)


### Getting bluetooth messages on linux

In order to get bluetooth to work on linux you may need to install bluez, bluez-utils-compat depending on the distro you use.

Here I do the following

1. power on the bluetooth adapter
2. scan for my device
3. pair to my device (aerial), default pin code is 1234
4. trust the device

```bash
user@user-UX310UQK:~$ bluetoothctl
[NEW] Controller 34:F3:9A:34:AA:43 user-UX310UQK [default]
Agent registered
[bluetooth]# power on
[CHG] Controller 34:F3:9A:34:AA:43 Class: 0x001c010c
Changing power on succeeded
[CHG] Controller 34:F3:9A:34:AA:43 Powered: yes
[bluetooth]# scan on
Discovery started
[CHG] Controller 34:F3:9A:34:AA:43 Discovering: yes
[NEW] Device 20:13:09:29:33:19 aerial
[bluetooth]# pair 20:13:09:29:33:19
Attempting to pair with 20:13:09:29:33:19
[CHG] Device 20:13:09:29:33:19 Connected: yes
Request PIN code
[aeri1m[agent] Enter PIN code: 1234
[CHG] Device 20:13:09:29:33:19 UUIDs: 00001101-0000-1000-8000-00805f9b34fb
[CHG] Device 20:13:09:29:33:19 ServicesResolved: yes
[CHG] Device 20:13:09:29:33:19 Paired: yes
Pairing successful
[bluetooth]# trust 20:13:09:29:33:19
[CHG] Device 20:13:09:29:33:19 Trusted: yes
Changing 20:13:09:29:33:19 trust succeeded
[bluetooth]# quit
Agent unregistered
[DEL] Controller 34:F3:9A:34:AA:43 user-UX310UQK [default]
```

Now bind the bluetooth monitor to `/dev/` and you are ready to go.

```bash
user@user-UX310UQK:~$ sudo rfcomm bind 0 20:13:09:29:33:19
[sudo] password for user:
user@user-UX310UQK:~$ file /dev/rfcomm0
/dev/rfcomm0: character special (216/0)
```

To connect to the newly opened `/dev/rfcomm0` use screen command:

``` bash
$ sudo screen /dev/rfcomm0 115200
```

You should now see the serial console output directly in your linux terminal. If you want to scroll the current serial output Ctrl-a and Esc, now you can scroll up and down. To terminate the current session press Ctrl-a and k and press y to terminate the session.

### Wiring to NodeMCU (ESP12E)

```
NodeMCU-----------HC06
3v3----------->VCC
GND--------->GND
TX----------->RXD
RX----------->TXD
```

In Atom IDE, you can see the serial monitor device:
![](file:///home/miha/workspace/www.popsblog.me/static/uploads/Screenshot_20190506_191556.jpeg)

And here is the output. I have the HC06 device connected to a NodeMCU.



