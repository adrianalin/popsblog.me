---
title: "A Look at Xiaomi Miio (Xiaomi Philips Smart Led Ball) Component From Home Assistant"
date: 2019-09-23T18:55:01+03:00
draft: false
tags: [ "homeassistant", "python" ]
---

Recently I bought a Xiaomi Philips smart bulb which works nicely with Home Assistant, so this would be a good post to describe how home assistant handles the connection and controls the light bulb.



![Light bulb](https://raw.githubusercontent.com/adrianalin/home-assistant_series/master/xiaomi_miio_smart_bulb/bulb.jpg)



To configure it with Home Assistant, I added the following to `configuration.yaml`:
```bash
light:
 - platform: xiaomi_miio
   name: Xiaomi Philips Smart Led Ball
   host: <your light bulb IP>
   token: <fill in your token>
   model: philips.light.bulb
```
`<fill in your token>` must be your actual token, here you find more details on how to get it [Retrieving the Access Token.](https://www.home-assistant.io/components/vacuum.xiaomi_miio/#retrieving-the-access-token)

### Requirements

Home Assistant uses `python-miio` this library (and its accompanying cli tool) is used to interface with devices using Xiaomi's [miIO protocol](https://github.com/OpenMiHome/mihome-binary-protocol/blob/master/doc/PROTOCOL.md). [Github repo](https://github.com/rytilahti/python-miio).


### Simple application to control the light bulb

The code to turn light on, turn light off, set brightness, set color temperature, set delayed turn off is quite simple using the `python-miio`:

```python
from miio import PhilipsBulb

light = PhilipsBulb("your_lightbulb_ip", "your_token")
light.on()
light.off()
light.set_color_temperature(20) # 1% - 100%
light.set_brightness(20) # 1% - 100%
state = light.status() # get status
light.delay_off(10) # auto turn off after n seconds
```

### Home Assistant way of doing it

The source file path in Home Assistant (as of version 0.98.5) which handles the light bulb connection is in `home-assistant/homeassistant/components/xiaomi_miio/light.py`

There is a hierarchy of classes used to control all kinds of Xiaomi Philips light devices like Moonlight Lamp, Eyecare Lamp, Ambient Light, Ceiling Lamp which have in common some more generic base classes.

![Home Assistant Xiaomi Philips lights class hierarchy](https://raw.githubusercontent.com/adrianalin/home-assistant_series/master/xiaomi_miio_smart_bulb/xiaomi_philips_diagram.png)

When Home Assistant starts, it calls `async_setup_platform()` with config info specific to this platform: `OrderedDict([('platform', 'xiaomi_miio'), ('name', 'Xiaomi Philips Smart Led Ball'), ('host', '<your_bulbs_ip>'), ('token', '<your bulb's token'>'), ('model', 'philips.light.bulb')])`. This is the info retrieved from `.homeassistant/configuration.yaml`.

Depending on the provided config (I only have the Philips Bulb configured() the `PhilipsBulb` class from `python-miio` gets instantiated, which does all the sockets communication with light bulb. Than is sent as argument to our `XiaomiPhilipsBulb` class, than gets registered with `hass.services`.

```python
async def async_setup_platform(hass, config, async_add_entities, discovery_info=None):
    """Set up the light from config."""
    from miio import Device, DeviceException

    host = config.get(CONF_HOST)
    name = config.get(CONF_NAME)
    token = config.get(CONF_TOKEN)
    model = config.get(CONF_MODEL)

	devices = []
    unique_id = None
    
    if model is None:
...
    elif model in [
        "philips.light.bulb",
        "philips.light.candle",
        "philips.light.candle2",
        "philips.light.downlight",
    ]:
        from miio import PhilipsBulb

        light = PhilipsBulb(host, token)
        device = XiaomiPhilipsBulb(name, light, model, unique_id)
        devices.append(device)
        hass.data[DATA_KEY][host] = device
...
```

Home Assistant periodically calls `async_update()`, to retrieve status changes. You can see below that `state = await self.hass.async_add_executor_job(self._light.status)` gets called. Than updates the class atributes with the info retrieved from the Bulb, like availability, if it's on/off, brightness level, color temperature value.

```python
class XiaomiPhilipsBulb(XiaomiPhilipsGenericLight):
...
    async def async_update(self):
        """Fetch state from the device."""
        from miio import DeviceException

        try:
            state = await self.hass.async_add_executor_job(self._light.status)
        except DeviceException as ex:
            self._available = False
            _LOGGER.error("Got exception while fetching the state: %s", ex)
            return

        _LOGGER.debug("Got new state: %s", state)
        self._available = True
        self._state = state.is_on
        self._brightness = ceil((255 / 100.0) * state.brightness)
        self._color_temp = self.translate(
            state.color_temperature, CCT_MIN, CCT_MAX, self.max_mireds, self.min_mireds
        )

        delayed_turn_off = self.delayed_turn_off_timestamp(
            state.delay_off_countdown,
            dt.utcnow(),
            self._state_attrs[ATTR_DELAYED_TURN_OFF],
        )

        self._state_attrs.update(
            {ATTR_SCENE: state.scene, ATTR_DELAYED_TURN_OFF: delayed_turn_off}
        )
```

The methods to control the light bulb are using sockets. This is a good use case for `asyncio` so all this functions are `async`. But `python-miio` library is not async compatible because the network requests are blocking in `python-miio`. `asyncio` has a remedy for that. It’s called executors. There is a pool of threads or processes dedicated for dealing with stuff incompatible with `asyncio`. In this use case are used threads, `asyncio` creates a pool of threads by default. To use it simply pass `None` as the first argument of `run_in_executor`. `functools.partial` is used here in order to pass the `*args` and `*kwargs` to `func`.

```python
...
self._loop = asyncio.get_running_loop()
...
self._loop.run_in_executor(None, partial(func, *args, **kwargs))
```

The `_try_command` method is the most important method of the class, all `platform-miio` related methods are handled by executor:

```python
class XiaomiPhilipsBulb:
    """Representation of a Xiaomi Philips Bulb."""
...
    async def _try_command(self, mask_error, func, *args, **kwargs):
        """Call a light command handling error messages."""
        from miio import DeviceException

        try:
            result = await self._loop.run_in_executor(None, partial(func, *args, **kwargs))
            _LOGGER.debug("Response received from light: %s", result)
            return result == SUCCESS
        except DeviceException as exc:
            _LOGGER.error(mask_error, exc)
            self._available = False
            return False
```

Let's take a look at:

```python
class XiaomiPhilipsBulb:
    """Representation of a Xiaomi Philips Bulb."""
...
    async def async_turn_on(self, **kwargs):
        """Turn the light on."""
        if ATTR_COLOR_TEMP in kwargs:
            color_temp = kwargs[ATTR_COLOR_TEMP]
            percent_color_temp = self.translate(
                color_temp, self.max_mireds, self.min_mireds, CCT_MIN, CCT_MAX
            )

        if ATTR_BRIGHTNESS in kwargs:
            brightness = kwargs[ATTR_BRIGHTNESS]
            percent_brightness = ceil(100 * brightness / 255.0)

        if ATTR_BRIGHTNESS in kwargs and ATTR_COLOR_TEMP in kwargs:
            _LOGGER.debug(
                "Setting brightness and color temperature: "
                "%s %s%%, %s mireds, %s%% cct",
                brightness,
                percent_brightness,
                color_temp,
                percent_color_temp,
            )

            result = await self._try_command(
                "Setting brightness and color temperature failed: " "%s bri, %s cct",
                self._light.set_brightness_and_color_temperature,
                percent_brightness,
                percent_color_temp,
            )

            if result:
                self._color_temp = color_temp
                self._brightness = brightness

        elif ATTR_COLOR_TEMP in kwargs:
            _LOGGER.debug(
                "Setting color temperature: " "%s mireds, %s%% cct",
                color_temp,
                percent_color_temp,
            )

            result = await self._try_command(
                "Setting color temperature failed: %s cct",
                self._light.set_color_temperature,
                percent_color_temp,
            )

            if result:
                self._color_temp = color_temp

        elif ATTR_BRIGHTNESS in kwargs:
            _LOGGER.debug("Setting brightness: %s %s%%", brightness, percent_brightness)

            result = await self._try_command(
                "Setting brightness failed: %s",
                self._light.set_brightness,
                percent_brightness,
            )

            if result:
                self._brightness = brightness

        else:
            await self._try_command("Turning the light on failed.", self._light.on)


async def main():
	"""Simple app to control the light bulb"""
	with open("config.json") as file:
        config = json.load(file)
    light = PhilipsBulb(config["host"], config["token"])
    device = XiaomiPhilipsBulb(config["name"], light, config["model"], None)
    await asyncio.gather(device.async_turn_on())


if __name__ == "__main__":
    asyncio.run(main())
```

1. When user flips the UI switch to turn on the light, `async_turn_on()` gets called without arguments and adds the `self._light.on` function call to async executor.

2. If user changes color temperature, triggers `async_turn_on()` with keyword arguments like `{'color_temp': 311}`, the function calculates the color temperature in percentage, and enters `elif ATTR_COLOR_TEMP in kwargs:` which adds `self._light.set_color_temperature` to async executor with calculated `percent_color_temp` as argument.

3. If user changes brightness, `async_turn_on()` gets called with keyword arguments like `{'brightness': 97}`, calculates brightness percentage, than calls `await self._try_command(self._light.set_brightness, percent_brightness,)`, which adds `self._light.set_brightness` to executor with `percent_brightness` as argument.

![](https://raw.githubusercontent.com/adrianalin/home-assistant_series/master/xiaomi_miio_smart_bulb/ui_controls.png)

All other aync member methods are the same, just adds the `python-miio` related methods to the async executor loop, and than asyncio takes care of scheduling the method for execution at the right time.

Supported function calls:

* `async_update()` schedules `self._light.status` for execution. Returns a `PhilipsBulbStatus` object that contains the following info `<PhilipsBulbStatus power=on, brightness=90, color_temperature=1, scene=0, delay_off_countdown=0>`

* `async_turn_off()` turns off the ligth

* `async_turn_on()` turns on the light

* `async_turn_on(color_temp=333)` sets the light temperature

* `async_turn_on(brightness=190)` sets the brightness

* `async_set_delayed_turn_off(timedelta(seconds=10))` auto turns off the light after 10 seconds

There are also some `@property` methods, used to retrieve info about the current state of light bulb.

For simplicity i just flattened all the functionality needed for the xiaomi philips light bulb in `XiaomiPhilipsBulb` class.

Project is located on github https://github.com/adrianalin/home-assistant_series/tree/master/xiaomi_miio_smart_bulb.