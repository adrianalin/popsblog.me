---
title: "Alsa on STM32F769-DISCO with wm8994 codec"
date: 2020-04-10T15:55:19+03:00
draft: false
tags: [ "linux", "buildroot" , "stm32"]
---

Codec configuration is done via I2C:

* I2C4: `PD12` (Alternate Function 4, `AUDIO_SCL`) and `PB7` (Alternate Function 11, `AUDIO_SDA` even if it is I2S4_SDA) on i2c address `0x1a` (detected with `i2cdetect` utility)

* SOC DAI (digital audio interface), it's a serial audio interface (SAI1 in our case). Provides audio data to the codec in the following formats: AC97, I2S, PCM (TDM, network mode).
`SAI1_A` block connection:
```bash
SOC             WM8994
PE4     ------> SAI1_FS_A
PE3     ------> SAI1_SD_B
PE5     ------> SAI1_SCK_A
PE6     ------> SAI1_SD_A
PG7     ------> SAI1_MCLK_A
```

#### Devicetree additions

In order for sound to work, enable in `linux-menuconfig` alsa related configs and codec related configs. Note that `SND_SOC_WM8994` is not available by default, i modified `/sound/soc/codecs/Kconfig` in order to show up in menuconfig. Also needed to manually enable `obj-y  += snd-soc-wm-hubs.o` in `sound/soc/codecs/Makefile`.

* in stm32f7-pinctrl.dtsi:
```bash
			pinctrl_sai1a: pinctrl_sai1a@0 {
				pins {
					pinmux = <STM32_PINMUX('G', 7, AF6)>,  /* SAI1_MCLK_A */
							<STM32_PINMUX('E', 3, AF6)>,  /* SAI1_SD_B */
							<STM32_PINMUX('E', 4, AF6)>,  /* SAI1_FS_A */
							<STM32_PINMUX('E', 5, AF6)>,  /* SAI1_SCK_A */
							<STM32_PINMUX('E', 6, AF6)>;  /* SAI1_SD_A */
				};
			};
```

* in stm32f746.dtsi:
```bash
		sai1: sai1@40015800 {
			compatible = "st,stm32h7-sai";
			#address-cells = <1>;
			#size-cells = <1>;
			ranges = <0 0x40015800 0x400>;
			reg = <0x40015800 0x4>;
			clocks = <&rcc 1 CLK_SAI1>, <&rcc 1 CLK_I2SQ_PDIV>, <&rcc 1 CLK_SAIQ_PDIV>;
			clock-names = "pclk", "x8k", "x11k";
			interrupts = <87>;
			status = "disabled";
		};
```

* in stm32f769-disco.dts:
```bash
	sound {
		compatible = "simple-audio-card";
		simple-audio-card,name = "my-soundcard";
		simple-audio-card,format = "i2s";
		simple-audio-card,bitclock-master = <&dailink0_master>;
		simple-audio-card,frame-master = <&dailink0_master>;

		dailink0_master: simple-audio-card,cpu {
			sound-dai = <&sai1a>;
			clocks = <&sai1a>;
		};

		simple-audio-card,codec {
			sound-dai = <&wm8994>;
			system-clock-frequency = <48000000>;
		};
	};
...

&i2c4 {
	pinctrl-0 = <&i2c4_pins>;
	pinctrl-names = "default";
	status = "okay";

	wm8994: wm8994@1a {
		compatible = "wlf,wm8994";
		reg = <0x1a>;
		#sound-dai-cells = <0>;
		gpio-controller;
		#gpio-cells = <2>;
		clocks = <&sai1a>;
		clock-names = "MCLK1";		//"MCLK1"
		status = "okay";

		// interrupt-parent = <&gpio>; no GPIO interrupts?
		// interrupts = <GPIO(J, 12) IRQ_TYPE_LEVEL_HIGH>;
	};
};
...

&sai1 {
	status = "okay";

	sai1a: audio-controller@40015804 {
		compatible = "st,stm32-sai-sub-a";
		reg = <0x4 0x1C>;
		#sound-dai-cells = <0>;
		#clock-cells = <0>; /* Set as master clock provider */
		clocks = <&rcc 1 CLK_SAI1>;
		clock-names = "sai_ck";
		dmas = <&dma1 2 4 0x10400 0x3>;
		dma-names = "tx"; /* SAI set as transmitter */
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_sai1a>;
		status = "okay";
	};

};
```

#### Debugging info

Useful documentation in kernel: `dpcm.rst`, `codec.rst`, `platform.rst`, `writing-an-alsa-driver.rsti`.

Useful information can be found in `debugfs`:

```bash
/sys/kernel/debug/asoc/STM32F769-DISCO/wm8994-codec/dapm # ls                                                          [10/1947]
bias_level             LINEOUT1P Driver       AIF2ADCR
AIF1 Playback          LINEOUT2N Driver       AIF2DACL
AIF1 Capture           LINEOUT2P Driver       AIF2DACR
AIF2 Playback          SPKOUTLP               AIF1DACDAT
AIF2 Capture           SPKOUTLN               AIF2DACDAT
AIF3 Playback          SPKOUTRP               AIF1ADCDAT
AIF3 Capture           SPKOUTRN               AIF2ADCDAT
IN1LN                  HPOUT1L                AIF1DAC Mux
IN1LP                  HPOUT1R                AIF2DAC Mux
IN2LN                  HPOUT2P                AIF2ADC Mux
IN2LP:VXRN             HPOUT2N                AIF3DACDAT
IN1RN                  LINEOUT1P              AIF3ADCDAT
IN1RP                  LINEOUT1N              TOCLK
IN2RN                  LINEOUT2P              DMIC2L
IN2RP:VXRP             LINEOUT2N              DMIC2R
MICBIAS2               DMIC1DAT               DMIC1L
MICBIAS1               DMIC2DAT               DMIC1R
IN1L PGA               Clock                  ADCL
IN1R PGA               MICBIAS Supply         ADCR
IN2L PGA               VMID                   AIF1 Loopback
IN2R PGA               CLK_SYS                AIF2 Loopback
MIXINL                 DSP1CLK                Debug log
MIXINR                 DSP2CLK                AIF3ADC Mux
Left Output Mixer      DSPINTCLK              AIF1CLK
Right Output Mixer     AIF1ADC1L              AIF2CLK
Left Output PGA        AIF1ADC1R              Late DAC1L Enable PGA
Right Output PGA       AIF1DAC1L              Late DAC1R Enable PGA
Headphone Supply       AIF1DAC1R              Late DAC2L Enable PGA
Headphone PGA          AIF1ADC2L              Late DAC2R Enable PGA
Earpiece Mixer         AIF1ADC2R              Direct Voice
Earpiece Driver        AIF1DAC2L              SPKL
SPKL Boost             AIF1DAC2R              SPKR
SPKR Boost             AIF1ADC1L Mixer        Left Headphone Mux
TSHUT                  AIF1ADC1R Mixer        Right Headphone Mux
SPKL Driver            AIF1ADC2L Mixer        Late Disable PGA
SPKR Driver            AIF1ADC2R Mixer        ADCL Mux
LINEOUT1 Mixer         AIF2DAC2L Mixer        ADCR Mux
LINEOUT2 Mixer         AIF2DAC2R Mixer        DAC2L
LINEOUT1N Mixer        Left Sidetone          DAC2R
LINEOUT1P Mixer        Right Sidetone         DAC1L
LINEOUT2N Mixer        DAC1L Mixer            DAC1R
LINEOUT2P Mixer        DAC1R Mixer
LINEOUT1N Driver       AIF2ADCL
```
which can be parsed using [vizdapm](git://opensource.wolfsonmicro.com/asoc-tools.git).

[result](https://raw.githubusercontent.com/adrianalin/STM32F769I-disco_Buildroot/master/result_2020.04.19-20.44.25.png)

This is the point I got stuck, codec is detected by kernel, as well as cpu interface and code interface, but couldn't get any sound on the speakers.

If you get:
`AIF1CLK not configured`

```bash
~ # cat /sys/kernel/debug/clk/clk_summary 
                                 enable  prepare  protect                                duty
   clock                          count    count    count        rate   accuracy phase  cycle
---------------------------------------------------------------------------------------------
 hsi                                  0        0        0    16000000     160000     0  50000
    hsi_div488                        0        0        0       32786     160000     0  50000
 clk-i2s-ckin                         0        0        0    48000000          0     0  50000
 clk-lsi                              0        0        0       32000          0     0  50000
    lsi                               0        0        0       32000          0     0  50000
 clk-lse                              0        0        0       32768          0     0  50000
    lse                               0        0        0       32768          0     0  50000
       hdmi-cec                       0        0        0       32768          0     0  50000
 clk-hse                              1        1        1    25000000          0     0  50000
    hse-rtc                           0        0        0     1000000          0     0  50000
    pll-src                           1        1        1    25000000          0     0  50000
       dsi                            0        0        0    25000000          0     0  50000
       vco_in                         1        2        2     1000000          0     0  50000
          vco-sai                     0        1        1   195000000          0     0  50000
             pllsai-r                 0        0        0    65000000          0     0  50000
                pllsai-r-div          0        0        0    32500000          0     0  50000
                   lcd-tft            0        0        0    32500000          0     0  50000
             pllsai-q                 0        1        0    48750000          0     0  50000
                pllsai-q-div          0        1        0    48750000          0     0  50000
                   sai2_clk           0        0        0    48750000          0     0  50000
                   sai1_clk           0        1        0    48750000          0     0  50000
                      adfsdm1         0        0        0    48750000          0     0  50000
             pllsai-p                 0        0        0    48750000          0     0  50000
                pll48                 0        0        0    48750000          0     0  50000
                   sdmux2             0        0        0    48750000          0     0  50000
                      sdmmc2          0        0        0    48750000          0     0  50000
                   sdmux1             0        0        0    48750000          0     0  50000
                      sdmmc1          0        0        0    48750000          0     0  50000
                   rng                0        0        0    48750000          0     0  50000
                   otgfs              0        0        0    48750000          0     0  50000

```

```bash
// codec registers
~ # cat /sys/kernel/debug/regmap/1-001a/registers
//sai registers
~ # cat /sys/kernel/debug/regmap/40015804.audio-controller
```

#### SAI

[TP8]  SAI_SCK_A/B		Input/output	Audio block A/B bit clock.
[TP22] SAI_MCLK_A/B 	Output 			Audio block A/B master clock.
[TP7]  SAI_SD_A/B 		Input/output 	Data line for block A/B.
[TP6]  SAI_FS_A/B 		Input/output 	Frame synchronization line for audio block A/B.


In master mode, the SAI delivers the timing signals to the external connected device:
• The bit clock and the frame synchronization are output on pin SCKx (to BCLKx, on schematic TP8) and FSx (to LRCLK, TP6), respectively.
• If needed, the SAI can also generate a master clock on MCLKx pin (to MCLK1, TP22).

The master clock can be generated externally on an I/O pad for external decoders if the corresponding audio block is declared as master with bit NODIV = 0 in the SAI_xCR1 register.

In master TX mode, enabling the audio block immediately generates the bit clock for the external slaves even if there is no data in the FIFO, However FS signal generation is conditioned by the presence of data in the FIFO. After the FIFO receives the first data to transmit, this data is output to external slaves. If there is no data to transmit in the FIFO, 0 values are then sent in the audio frame with an underrun flag generation.

When a invalid slot is transferred, the SD data line is either forced to 0 or released to HI-z depending on TRIS bit configuration (refer to Output data line management on an inactive slot) in transmitter mode. In receiver mode, the received value from the end of this slot is ignored. Consequently, there will be no FIFO access and so no request to read or write the FIFO linked to this inactive slot status.

In master TX mode, enabling the audio block immediately generates the bit clock for the external slaves even if there is no data in the FIFO, However FS signal generation is conditioned by the presence of data in the FIFO. After the FIFO receives the first data to transmit, this data is output to external slaves. If there is no data to transmit in the FIFO, 0 values are then sent in the audio frame with an underrun flag generation.

stm32_sai_mclk_enable() enable MCLK: SAI_XCR1_NODIV is 0
stm32_sai_set_sysclk()

To configure the audio sub-block for DMA transfer, set DMAEN bit in the SAI_xCR1 register.

The audio frame can target different data sizes by configuring bit DS[2:0] in the SAI_xCR1.
Bit 8 LSBFIRST: 0: Data are transferred with MSB first.
Bits 7:5. DS[2:0]: Data size. 100: 16 bits (depends on hw_params)

SAI_AFRCR = 00050f1f
Bit 16 FSDEF: Frame synchronization definition. 1: FS signal is a start of frame signal + channel side identification. (I2S)
When the FS signal is considered as a start of frame and channel side identification within the frame, the number of declared slots must be considered to be half the number for the left channel and half the number for the right channel. If the number of bit clock cycles on half audio frame is greater than the number of slots dedicated to a channel side, and TRIS = 0, 0 is sent for transmission for the remaining bit clock cycles in the SAI_xCR2 register. Otherwise if TRIS = 1, the SD line is released to HI-Z. In reception mode, the remaining bit clock cycles are not considered until the channel side changes.
Bits 7:0 FRL[7:0]: Frame length. = 0x11111 = 31
These bits are set and cleared by software. They define the audio frame length expressed in number of SCK clock cycles: the number of bits in the frame is equal to FRL[7:0] + 1.


SAI_xSLOTR = 0x00030100
NBSLOT[3:0] = 0x1. The value set in this bitfield represents the number of slots + 1 in the audio frame (including the number of inactive slots). The maximum number of slots is 16.
Bits 7:6 SLOTSZ[1:0]: Slot size. 00: The slot size is equivalent to the data size (specified in DS[3:0] in the SAI_xCR1 register).
When a invalid slot is transferred, the SD data line is either forced to 0 or released to HI-z depending on TRIS bit configuration (refer to Output data line management on an inactive slot) in transmitter mode. In receiver mode, the received value from the end of this slot is ignored. Consequently, there will be no FIFO access and so no request to read or write the FIFO linked to this inactive slot status.

### WM8994
In master interface mode, the clock signals BCLK and LRCLK are outputs from the WM8994. In slave mode, these signals are inputs, as illustrated below.

The Digital Audio Interface AIF1 can operate in Master or Slave modes, selected by AIF1_MSTR. In Master mode, the BCLK1 and LRCLK1 signals are generated by the WM8994 when one or more AIF1 channels is enabled.

### DMA

To free the CPU and to optimize bus bandwidth, each SAI audio block has an independent DMA interface to read/write from/to the SAI_xDR register (to access the internal FIFO).

DMA_SxCR: 0x06a54c54 vs 0x00a54c54
Note: Before setting EN bit to '1' to start a new transfer, the event flags corresponding to the stream in DMA_LISR or DMA_HISR register must be cleared.

DIR[1:0]: 	01: Memory-to-peripheral DMA_SxM0AR
01 Memory-to-peripheral DMA_SxM0AR -> DMA_SxPAR

PINC: 		0: peripheral address pointer is fixed
MINC: 		1: memory address pointer is incremented after each data transfer (increment is done according to MSIZE)
PSIZE[1:0]: peripheral data size 01: half-word (16-bit); These bits are protected and can be written only if EN is ‘0’.
MSIZE[1:0]: memory data size 10: word (32-bit). These bits are protected and can be written only if EN is ‘0’. In direct mode, MSIZE is forced by hardware to the same value as PSIZE as soon as bit EN = '1'.
PINCOS: peripheral increment offset size. 0: The offset size for the peripheral address calculation is linked to the PSIZE
PL[1:0]: priority level. 01: medium
DBM: double-buffer mode. 1: memory target switched at the end of the DMA transfer
CT: current target (only in double-buffer mode). 0: current target memory is Memory 0 (addressed by the DMA_SxM0AR pointer)
PBURST[1:0]: peripheral burst transfer configuration. 01: INCR4 (incremental burst of 4 beats). In direct mode, these bits are forced to 0x0 by hardware.
MBURST[1:0]: memory burst transfer configuration 01: INCR4 (incremental burst of 4 beats). In direct mode, these bits are forced to 0x0 by hardware as soon as bit EN= '1'.
CHSEL[3:0]: channel selection. 0011: channel 3 selected.

DMA_SxNDTR: 0x00000780 (1920)
Bits 15:0 NDT[15:0]: number of data items to transfer (0 up to 65535)
This register can be written only when the stream is disabled. When the stream is enabled, this register is read-only, indicating the remaining data items to be transmitted. This register decrements after each DMA transfer. Once the transfer is completed, this register can either stay at zero (when the stream is in normal mode) or be reloaded automatically with the previously programmed value in the following cases: – when the stream is configured in circular mode. – when the stream is enabled again by setting EN bit to '1'. If the value of this register is zero, no transaction can be served even if the stream is enabled.

SPAR:  0x40015820 = SAI_ADR - data. A write to this register loads the FIFO provided the FIFO is not full.
PAR[31:0]: peripheral address Base address of the peripheral data register from/to which the data is read/written. These bits are write-protected and can be written only when bit EN = '0' in the DMA_SxCR register.

SM0AR: 0xc0f00000 (Reserved memory: created DMA memory pool at 0xc0f00000, size 1 MiB)
M0A[31:0]: memory 0 address Base address of memory area 0 from/to which the data is read/written. These bits are write-protected. They can be written only if: – the stream is disabled (bit EN= '0' in the DMA_SxCR register) or – the stream is enabled (EN=’1’ in DMA_SxCR register) and bit CT = '1' in the DMA_SxCR register (in double-buffer mode).

```
Reserved memory: created DMA memory pool at 0xc0f00000, size 1 MiB
```

https://www.alsa-project.org/wiki/FramesPeriods
960(20ms, period or frame count) * 2 (buffer_period_count) = 1920
A period is the number of frames in between each hardware interrupt. The poll() will return once a period.
0xc0f00f00 - 0xc0f00000 = 0xf00 = 3840 = (DMA_SxNDTR)1920 * 2

SM1AR: 0xc0f00f00
M1A[31:0]: memory 1 address (used in case of double-buffer mode) Base address of memory area 1 from/to which the data is read/written. This register is used only for the double-buffer mode. These bits are write-protected. They can be written only if: – the stream is disabled (bit EN= '0' in the DMA_SxCR register) or – the stream is enabled (EN=’1’ in DMA_SxCR register) and bit CT = '0' in the DMA_SxCR register.

SFCR:  0x000000a7 = 0b10100111
FTH[1:0]: FIFO threshold selection. 11: full FIFO.
DMDIS: direct mode disable. 1: direct mode disabled.
Bits 5:3 FS[2:0]: FIFO status. 100: FIFO is empty
Bit 7 FEIE: FIFO error interrupt enable. FE interrupt enabled

A stacktrace when calling alsa-lib function `snd_pcm_writei()`:
```bash
#0  stm32_sai_trigger (substream=<optimized out>, cmd=1, cpu_dai=0xc0b5c2c0) at sound/soc/stm/stm32_sai_sub.c:1124
#1  0xc016967a in soc_pcm_trigger_start (cmd=<optimized out>, substream=<optimized out>) at sound/soc/soc-pcm.c:1213
#2  soc_pcm_trigger (substream=0xc0c28740, cmd=1) at sound/soc/soc-pcm.c:1255
#3  0xc015b410 in snd_pcm_action_single (ops=0xc0219140 <snd_pcm_action_start>, substream=0xc0c28740, state=3) at sound/core/pcm_native.c:1197
#4  0xc015c606 in snd_pcm_action (ops=<optimized out>, substream=<optimized out>, state=<optimized out>) at sound/core/pcm_native.c:1280
#5  0xc015d256 in snd_pcm_start (substream=<optimized out>) at sound/core/pcm_native.c:1381
#6  0xc0160a8a in __snd_pcm_lib_xfer (substream=0xc0c28740, data=<optimized out>, interleaved=<optimized out>, size=0, in_kernel=<optimized out>)
    at sound/core/pcm_lib.c:2246
#7  0xc015dbb4 in snd_pcm_lib_write (frames=<optimized out>, buf=<optimized out>, substream=<optimized out>) at ./include/sound/pcm.h:1078
#8  snd_pcm_xferi_frames_ioctl (_xferi=<optimized out>, substream=<optimized out>) at sound/core/pcm_native.c:3095
#9  snd_pcm_common_ioctl (arg=<optimized out>, cmd=<optimized out>, substream=<optimized out>, file=<optimized out>) at sound/core/pcm_native.c:3252
#10 snd_pcm_ioctl (file=0xc055aba0, cmd=1074544976, arg=3227917612) at sound/core/pcm_native.c:3275
#11 0xc006f024 in vfs_ioctl (filp=<optimized out>, cmd=<optimized out>, arg=<optimized out>) at fs/ioctl.c:47
#12 0xc006f492 in ksys_ioctl (fd=6, cmd=1074544976, arg=3227917612) at fs/ioctl.c:763
#13 0xc00081c0 in __idmap_text_end ()
```


 * Scan each dapm widget for complete audio path.
  * A complete path is a route that has valid endpoints i.e.:-
   *
    *  o DAC to output pin.
	 *  o Input pin to ADC.
	  *  o Input pin to Output pin (bypass, sidetone)
	   *  o DAC to ADC (loopback).

WM8994 registers:

TOCLK(208h) - timoutclok bit (is set on STM disco)

0039: 006c Enable VMID soft start (fast), Start-up Bias Current Enabled
0001: 0303 Enables HPOUT1L input stage; Enables HPOUT1R input stage; 2 x 40k divider (Normal mode); Enables the Normal bias current generator (for all analogue functions);

0005: 0303 vs 0x0F0F AIF1DAC1L_ENA AIF1DAC1R_ENA DAC1L_ENA DAC1R_ENA
0601: 0001 Enable AIF1 (Timeslot 0, Left) to DAC1L
0602: 0001 Enable AIF1 (Timeslot 1, Right) to DAC1R
0604: 0000 Disable the AIF1 Timeslot 1 (Left) to DAC 2 (Left) mixer path
0605: 0000 Disable the AIF1 Timeslot 1 (Right) to DAC 2 (Right) mixer path

0210: 0083 -> AIF1 Sample Rate = 48 (KHz), ratio=256
0300: 6010 vs 4010 -> AIF1 Word Length = 16-bits, AIF1 Format = I2S
0302: 0000 -> slave mode
0208: 001a vs 0x000A -> TOCLK_ENA, AIF1DSPCLK_ENA, SYSDSPCLK_ENA
0200: 0001 -> AIF1CLK_ENA, AIF1CLK Source Select 00 = MCLK1
0003: 00f0 vs 0x0300 -> MIXOUTLVOL_ENA MIXOUTRVOL_ENA MIXOUTL_ENA MIXOUTR_ENA
0022: 0003 vs 0x0000 -> Left Speaker Mixer Volume Control: Mute
0023: 0003 vs 0x0000 -> Right Speaker Mixer Volume Control: Mute
0036: 0000 vs 0x0300 -> Left DAC2 to SPKMIXL Mute. Right DAC2 to SPKMIXR Mute
0001: 0303 vs 0x3003 -> Enables HPOUT1L input stage. Enables HPOUT1R input stage. Enables the Normal bias current generator (for all analogue functions). 2 x 40k divider (for normal operation).
0051: 0007 vs 0x0005 -> Selects the digital audio source for envelope tracking  Enable dynamic charge pump power control -> charge pump controlled by real-time audio level ???
0001: 0303 vs 0x0303 | 0x3003;
0060: 00ee vs 0x0022 -> HPOUT1 (Left) and HPOUT1 (Right) intermediate stages
004c: 9f25 Enable Charge Pump
002d: 0001 Select DAC1 (Left) to Left Headphone Output PGA (HPOUT1LVOL) path
002e: 0001 Select DAC1 (Right) to Right Headphone Output PGA (HPOUT1RVOL) path
0003: 00f0 vs 0x0030 | 0x0300
0054: 0000 vs 0x0033 ?? sould Enable DC Servo and trigger start-up mode on left and right channels

0610: 01b4 vs 0x00C0 DAC1L Soft Mute Control 1 = DAC Mute ???
0611: 01b4 same as above
0420: 0000 Unmute the AIF1 Timeslot 0 DAC path

0612: 03c0 vs 0x00C0 DAC2L Soft Mute Control 1 = DAC Mute ???
0613: 03c0 vs 0x00C0 same as above
0422: 0000 Unmute the AIF1 Timeslot 1 DAC2 path

-- set volume
0420: 0000 Unmute the AIF1 Timeslot 0 DAC1 path L&R
0422: 0000 Unmute the AIF1 Timeslot 1 DAC2 path L&R
001c: 01ff vs 0x3F | 0x140 Left Headphone Volume
001d: 01ff vs 0x3F | 0x140 Right Headphone Volume
0026: 0179 vs 0x3F | 0x140 Left Speaker Volume
0027: 0179 vs 0x3F | 0x140 Right Speaker Volume


#### `amixer` commands

On an embedded system, it can be enough to simply set the controls once using `alsamixer` or `amixer` and then save the configuration with `alsactl store` (see `/etc/init.d/alsa-utils`). This saves the driver state to the configuration file which, by default, is `/var/lib/alsa/asound.state`.

These commands are run on Raspberry Pi:

```bash
pi@raspberrypi:/var/lib/alsa $ cat asound.state 
state.b1 {
	control.1 {
		iface MIXER
		name 'HDMI Playback Volume'
		value 1
		comment {
			access 'read write'
			type INTEGER
			count 1
			range '-10239 - 400'
			dbmin -9999999
			dbmax 400
			dbvalue.0 1
		}
	}
	control.2 {
		iface MIXER
		name 'HDMI Playback Switch'
		value true
		comment {
			access 'read write'
			type BOOLEAN
			count 1
		}
	}
}
state.Headphones {
	control.1 {
		iface MIXER
		name 'Headphone Playback Volume'
		value 20
		comment {
			access 'read write'
			type INTEGER
			count 1
			range '-10239 - 400'
			dbmin -9999999
			dbmax 400
			dbvalue.0 20
		}
	}
	control.2 {
		iface MIXER
		name 'Headphone Playback Switch'
		value true
		comment {
			access 'read write'
			type BOOLEAN
			count 1
		}
	}
}

pi@raspberrypi:/var/lib/alsa $ amixer controls
numid=2,iface=MIXER,name='Headphone Playback Switch'
numid=1,iface=MIXER,name='Headphone Playback Volume'
pi@raspberrypi:/var/lib/alsa $ amixer cset name='Headphone Playback Switch' 0
numid=2,iface=MIXER,name='Headphone Playback Switch'
  ; type=BOOLEAN,access=rw------,values=1
  : values=off
pi@raspberrypi:/var/lib/alsa $ amixer cset name='Headphone Playback Volume' 50
numid=1,iface=MIXER,name='Headphone Playback Volume'
  ; type=INTEGER,access=rw---R--,values=1,min=-10239,max=400,step=0
  : values=50
  | dBscale-min=-102.39dB,step=0.01dB,mute=1
```

The controls are defined in raspberrypi4-64/kernel-source/drivers/staging/vc04_services/bcm2835-audio/bcm2835-ctl.c and looks like this:
```C
static const struct snd_kcontrol_new snd_bcm2835_headphones_ctl[] = {
	{
		.iface = SNDRV_CTL_ELEM_IFACE_MIXER,
		.name = "Headphone Playback Volume",
		.index = 0,
		.access = SNDRV_CTL_ELEM_ACCESS_READWRITE |
			  SNDRV_CTL_ELEM_ACCESS_TLV_READ,
		.private_value = PCM_PLAYBACK_VOLUME,
		.info = snd_bcm2835_ctl_info,
		.get = snd_bcm2835_ctl_get,
		.put = snd_bcm2835_ctl_put,
		.count = 1,
		.tlv = {.p = snd_bcm2835_db_scale}
	},
	{
		.iface = SNDRV_CTL_ELEM_IFACE_MIXER,
		.name = "Headphone Playback Switch",
		.index = 0,
		.access = SNDRV_CTL_ELEM_ACCESS_READWRITE,
		.private_value = PCM_PLAYBACK_MUTE,
		.info = snd_bcm2835_ctl_info,
		.get = snd_bcm2835_ctl_get,
		.put = snd_bcm2835_ctl_put,
		.count = 1,
	}
};

static const struct snd_kcontrol_new snd_bcm2835_hdmi[] = {
	{
		.iface = SNDRV_CTL_ELEM_IFACE_MIXER,
		.name = "HDMI Playback Volume",
		.index = 0,
		.access = SNDRV_CTL_ELEM_ACCESS_READWRITE |
			  SNDRV_CTL_ELEM_ACCESS_TLV_READ,
		.private_value = PCM_PLAYBACK_VOLUME,
		.info = snd_bcm2835_ctl_info,
		.get = snd_bcm2835_ctl_get,
		.put = snd_bcm2835_ctl_put,
		.count = 1,
		.tlv = {.p = snd_bcm2835_db_scale}
	},
	{
		.iface = SNDRV_CTL_ELEM_IFACE_MIXER,
		.name = "HDMI Playback Switch",
		.index = 0,
		.access = SNDRV_CTL_ELEM_ACCESS_READWRITE,
		.private_value = PCM_PLAYBACK_MUTE,
		.info = snd_bcm2835_ctl_info,
		.get = snd_bcm2835_ctl_get,
		.put = snd_bcm2835_ctl_put,
		.count = 1,
	}
};
```
There is also a nice example on how to change the controls using alsa-lib API https://bootlin.com/blog/configuring-alsa-controls-from-an-application/