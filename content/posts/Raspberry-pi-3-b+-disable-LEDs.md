---
title: "Raspberry Pi 3 B+ Disable LEDs"
date: 2019-11-08T18:55:02+03:00
draft: false
tags: [ "raspberrypi" ]
---

If you are using your Raspberry Pi as a TV Media Center, like I do, and it sits bellow your TV, you may want to disable the LEDs. Luckily there is a way to do this: you need to edit `nano /boot/config.txt` (with `sudo`):
```bash
# Disable the ACT LED.
dtparam=act_led_trigger=none
dtparam=act_led_activelow=on

# Disable the PWR LED.
dtparam=pwr_led_trigger=none
dtparam=pwr_led_activelow=on
```


### Raspberry Pi model 2 B, B+ and A+

Add the following to `/boot/config.txt`:
```bash
# Disable the ACT LED.
dtparam=act_led_trigger=none
dtparam=act_led_activelow=off

# Disable the PWR LED.
dtparam=pwr_led_trigger=none
dtparam=pwr_led_activelow=off
```

### Raspberry Pi Zero

Add the following to `/boot/config.txt`:
```bash
# Disable the ACT LED on the Pi Zero.
dtparam=act_led_trigger=none
dtparam=act_led_activelow=on
```


### Enable linux serial console

In order to login to raspberry pi via serial console you need to edit  `/boot/config.txt`, and add `console=serial0,115200`. After editing, my `/boot/config.txt` looks like:
```bash
dwc_otg.lpm_enable=0 console=serial0,115200 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait quiet logo.nologo loglevel=0 vt.global_cursor_default=0 noswap splash plymouth.ignore-serial-consoles consoleblank=0 ipv6.disable=1
```