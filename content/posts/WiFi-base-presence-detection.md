---
title: "WiFi Base Presence Detection"
date: 2019-10-30T18:55:02+03:00
draft: false
tags: [ "homeassistant", "python" ]
---

There are multiple ways to implement presence detection in your smart home. The most common presence detection methods in Home Assistant are based on your phone: bluetooth, WiFi (router querying, `nmap`), GPS. Here I will look at code for WiFi based presence detection, precisely:

* querying a UPC WiFi router (Compal CH7465LG) to get a list of connected device, like Home Assistant does

![Router](https://raw.githubusercontent.com/adrianalin/home-assistant_series/master/wifi_presence_detection/compal.png)

* `nmap` is an open source tool used to discover hosts and services on a computer network by sending packets and analyzing the responses

### Device tracking in Home Assistant

There is the `DeviceScanner` base class:
```python
class DeviceScanner:
    """Device scanner object."""

    hass: HomeAssistantType = None

    def scan_devices(self) -> List[str]:
        """Scan for devices."""
        raise NotImplementedError()

    def async_scan_devices(self) -> Any:
        """Scan for devices.

        This method must be run in the event loop and returns a coroutine.
        """
        return self.hass.async_add_job(self.scan_devices)

    def get_device_name(self, device: str) -> str:
        """Get the name of a device."""
        raise NotImplementedError()

    def async_get_device_name(self, device: str) -> Any:
        """Get the name of a device.

        This method must be run in the event loop and returns a coroutine.
        """
        return self.hass.async_add_job(self.get_device_name, device)

    def get_extra_attributes(self, device: str) -> dict:
        """Get the extra attributes of a device."""
        raise NotImplementedError()

    def async_get_extra_attributes(self, device: str) -> Any:
        """Get the extra attributes of a device.

        This method must be run in the event loop and returns a coroutine.
        """
        return self.hass.async_add_job(self.get_extra_attributes, device)
```

This class serves as interface for all kinds of different devices that can provide presence info (there are routers, or services): `SwisscomDeviceScanner`, `DdWrtDeviceScanner`, `ActiontecDeviceScanner`, `FritzBoxScanner`, `MockScanner`, `BboxDeviceScanner`, `LuciDeviceScanner`, `AsusWrtDeviceScanner`, `CiscoDeviceScanner`, `Icloud`, `FreeboxDeviceScanner`, `TplinkDeviceScanner`, `Tplink1DeviceScanner`, `NetgearDeviceScanner`, `SkyHubDeviceScanner`, `ThomsonDeviceScanner`, `UPCDeviceScanner`, `NmapDeviceScanner` and more.

In this post I will only take a look at `UPCDeviceScanner`, and `NmapDeviceScanner`.

### UPC WiFi router (Compal CH7465LG) based device tracking

To configure the UPC WiFi router presence detection, I added the following to my `/home/user/.homeassistant/configuration.yaml`:
```bash
device_tracker:
  - platform: upc_connect
    password: !secret upc_password # router admin password
    host: <router_ip>
    new_device_defaults:
      track_new_devices: true
```
This config is used by `UPCDeviceScanner` in `homeassistant/components/upc_connect/device_tracker.py`.


### Requirements

The `UPCDeviceScanner` makes use of `python-connect-box` package. On the github page is described as a `Python Client for interacting with the cable modem/router Compal CH7465LG which is provided under different names by various ISP in Europe.` <a href="https://github.com/fabaff/python-connect-box">https://github.com/fabaff/python-connect-box</a>

### `UPCDeviceScanner` class

When `homeassistant` starts, `async_get_scanner` gets called. Here a new instance of `ConnectBox` (from `python-connect-box` package) is created, using the provided config. This `connect_box` instance is passed as an argument to `UPCDeviceScanner` class.
```python
async def async_get_scanner(hass, config):
    """Return the UPC device scanner."""
    conf = config[DOMAIN]
    session = hass.helpers.aiohttp_client.async_get_clientsession()
    connect_box = ConnectBox(session, conf[CONF_PASSWORD], host=conf[CONF_HOST])

    # Check login data
    try:
        await connect_box.async_initialize_token()
    except ConnectBoxLoginError:
        _LOGGER.error("ConnectBox login data error!")
        return None
    except ConnectBoxError:
        pass

    async def _shutdown(event):
        """Shutdown event."""
        await connect_box.async_close_session()

    hass.bus.async_listen_once(EVENT_HOMEASSISTANT_STOP, _shutdown)

    return UPCDeviceScanner(connect_box)
```

`UPCDeviceScanner` class inherits `DeviceScanner`, and overrides:

* `async_scan_devices` that returns a list of MAC addresses of connected devices
* `async_get_device_name` should get the device name for specified MAC address

```python
class UPCDeviceScanner(DeviceScanner):
    """This class queries a router running UPC ConnectBox firmware."""

    def __init__(self, connect_box: ConnectBox):
        """Initialize the scanner."""
        self.connect_box: ConnectBox = connect_box

    async def async_scan_devices(self) -> List[str]:
        """Scan for new devices and return a list with found device IDs."""
        try:
            await self.connect_box.async_get_devices()
        except ConnectBoxError:
            return []

        return [device.mac for device in self.connect_box.devices]

    async def async_get_device_name(self, device: str) -> Optional[str]:
        """Get the device name (the name of the wireless device not used)."""
        for connected_device in self.connect_box.devices:
            if connected_device != device:
                continue
            return connected_device.hostname

        return None
```

Note that `UPCDeviceScanner` class is a wraper around the `ConnectBox` class (from `python-connect-box` package), which handles communication with WiFi router device. This class executes commands on UPC firmware webservice, I really recommend you take a look at this class just to get an idea of how the requests are made.

### `Nmap` based device tracking

As an alternative to the router-based device tracking, it is possible to directly scan the network for devices by using `nmap`.
To configure it add to `configuration.yaml`:
```bash
device_tracker:
  - platform: nmap_tracker
    hosts: 192.168.1.0/24 # change accordingly
```

### Requirements

`python-nmap`  is a python library which helps in using `nmap` port scanner. It allows to easilly manipulate nmap scan results and will be a perfect tool for systems administrators who want to automatize scanning task and reports [[https://pypi.org/project/python-nmap/]](https://layers.openembedded.org/layerindex/branch/master/layers/). This tool is built around the `nmap` command line utility, and uses `subprocess` to launch it:
```python
p = subprocess.Popen(args, bufsize=100000,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
```
where `args` looks like this: `<class 'list'>: ['nmap', '-oX', '-', '192.168.0.0/24', '-F', '--host-timeout', '5s']`, argument `-oX` specify the output format to be `xml`, beeing parsed by the package.

### The `NmapDeviceScanner` class

This class also inherits from `DeviceScanner` and overrides `scan_devices` and `get_device_name`.

```python
class NmapDeviceScanner(DeviceScanner):
    """This class scans for devices using nmap."""

    exclude = []

    def __init__(self, config):
        """Initialize the scanner."""
        self.last_results = []

        self.hosts = config[CONF_HOSTS]
        self.exclude = config[CONF_EXCLUDE]
        minutes = config[CONF_HOME_INTERVAL]
        self._options = config[CONF_OPTIONS]
        self.home_interval = timedelta(minutes=minutes)

        _LOGGER.debug("Scanner initialized")

    def scan_devices(self):
        """Scan for new devices and return a list with found device IDs."""
        self._update_info()

        _LOGGER.debug("Nmap last results %s", self.last_results)

        return [device.mac for device in self.last_results]

    def get_device_name(self, device):
        """Return the name of the given device or None if we don't know."""
        filter_named = [
            result.name for result in self.last_results if result.mac == device
        ]

        if filter_named:
            return filter_named[0]
        return None

    def get_extra_attributes(self, device):
        """Return the IP of the given device."""
        filter_ip = next(
            (result.ip for result in self.last_results if result.mac == device), None
        )
        return {"ip": filter_ip}

    def _update_info(self):
        """Scan the network for devices.

        Returns boolean if scanning successful.
        """
        _LOGGER.debug("Scanning...")

        from nmap import PortScanner, PortScannerError

        scanner = PortScanner()

        options = self._options

        if self.home_interval:
            boundary = datetime.now() - self.home_interval
            last_results = [
                device for device in self.last_results if device.last_update > boundary
            ]
            if last_results:
                exclude_hosts = self.exclude + [device.ip for device in last_results]
            else:
                exclude_hosts = self.exclude
        else:
            last_results = []
            exclude_hosts = self.exclude
        if exclude_hosts:
            options += " --exclude {}".format(",".join(exclude_hosts))

        try:
            result = scanner.scan(hosts=" ".join(self.hosts), arguments=options)
        except PortScannerError:
            _LOGGER.error("PortScannerError happened!")
            return False

        now = datetime.now()
        for ipv4, info in result["scan"].items():
            if info["status"]["state"] != "up":
                continue
            name = info["hostnames"][0]["name"] if info["hostnames"] else ipv4
            # Mac address only returned if nmap ran as root
            mac = info["addresses"].get("mac")  # or get_mac_address(ip=ipv4)
            if mac is None:
                _LOGGER.info("No MAC address found for %s", ipv4)
                continue
            last_results.append(Device(mac.upper(), name, ipv4, now))

        self.last_results = last_results

        _LOGGER.debug("nmap scan successful")
        return True
```
The `NmapDeviceScanner` constructor takes the config as argument. In config there is info like `hosts` (nmap scans given hosts, eg `192.168.0.0/24`), `options` (string of arguments for nmap, eg `-F --host-timeout 5s`).

The most important method is `_update_info()` which instantiates the `PortScanner` class from `python-nmap`. Than `result = scanner.scan(hosts=" ".join(self.hosts), arguments=options)` is called, than parses the results and appends them in a list of `Device = namedtuple("Device", ["mac", "name", "ip", "last_update"])` items.
The `scan_devices()` method uses the results list to return a list of devices MAC addresses.
The `get_device_name()` method is used to get the name of a device with a specific MAC address.

Here is a small working example <a href="https://github.com/adrianalin/home-assistant_series/tree/master/wifi_presence_detection">https://github.com/adrianalin/home-assistant_series/tree/master/wifi_presence_detection</a>.