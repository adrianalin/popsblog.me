---
title: "Controlling a WiFi RGB Light strip with flux_led component from Home Assistant"
date: 2019-09-27T18:55:01+03:00
draft: false
tags: [ "homeassistant", "python" ]
---

This post is about home assistant's `flux_led` component, used to control several brands of bulbs and controllers that use the same protocol as the MagicHome app. I have a cheap chinese RGB LED strip with a WiFi controller that works great with home assistant: ZDM WiFi Smart RGB LED Controller.

![](https://raw.githubusercontent.com/adrianalin/home-assistant_series/master/wifi_rgb_led_strip/wifi_controller.jpg)

### Requirements

In your virtualenv do `pip install flux-led` to install the Python library (Version 0.22) used to communicate with the `flux_led` smart bulbs. `The protocol was reverse-engineered by studying packet captures between a bulb and the controlling "Magic Home" mobile app. The code here dealing with the network protocol is littered with magic numbers, and ain't so pretty. But it does seem to work!` [[https://github.com/beville/flux_led]](https://github.com/beville/flux_led)

### Home assistant component

The `flux_led` component is located at `homeassistant/components/flux_led`. When home assistant starts, it calls `setup_platform()` for each component. For `flux_led` component `setup_platform()` looks like this:

```python
def setup_platform(hass, config, add_entities, discovery_info=None):
    """Set up the Flux lights."""
    import flux_led

    lights = []
    light_ips = []

	# if there is any flux_led configured, use the provided info
	...

    # Otherwise Find the bulbs on the LAN
    scanner = flux_led.BulbScanner()
    scanner.scan(timeout=10)
    for device in scanner.getBulbInfo():
        ipaddr = device["ipaddr"]
        if ipaddr in light_ips:
            continue
        device["name"] = "{} {}".format(device["id"], ipaddr)
        device[ATTR_MODE] = None
        device[CONF_PROTOCOL] = None
        device[CONF_CUSTOM_EFFECT] = None
        light = FluxLight(device)
        lights.append(light)

    add_entities(lights, True)
```

If there is any config provided for `flux_led`  in `.homeassistant/configuration.yaml` use that config, otherwise use `flux_led.BulbScanner()` to scan LAN for `flux_led` devices (I used `automatic_add: true`). The `FluxLight(device)` gets instantiated, and constructor is called:

```python
class FluxLight(Light):
    """Representation of a Flux light."""

    def __init__(self, device):
        """Initialize the light."""
        self._name = device["name"]
        self._ipaddr = device["ipaddr"]
        self._protocol = device[CONF_PROTOCOL]
        self._mode = device[ATTR_MODE]
        self._custom_effect = device[CONF_CUSTOM_EFFECT]
        self._bulb = None
        self._error_reported = False
```

Home assistant regularly calls `update()` on each component:

```python
class FluxLight(light):
...
    def update(self):
        """Synchronize state with bulb."""
        if not self.available:
            try:
                self._connect()
                self._error_reported = False
            except socket.error:
                self._disconnect()
                if not self._error_reported:
                    _LOGGER.warning(
                        "Failed to connect to bulb %s, %s", self._ipaddr, self._name
                    )
                    self._error_reported = True
                return

        self._bulb.update_state(retry=2)
```

If the RGB controller communication it's not configured yet, get's configured in `self._connect()`. If already configured (or after configuring), it calls `self._bulb.update_state(retry=2)`.

```python
class FluxLight(Light):
...
    def _connect(self):
        """Connect to Flux light."""
        import flux_led

        self._bulb = flux_led.WifiLedBulb(self._ipaddr, timeout=5)
        if self._protocol:
            self._bulb.setProtocol(self._protocol)

        # After bulb object is created the status is updated. We can
        # now set the correct mode if it was not explicitly defined.
        if not self._mode:
            if self._bulb.rgbwcapable:
                self._mode = MODE_RGBW
            else:
                self._mode = MODE_RGB
```

In `_connect()`, the `flux_led.WifiLedBulb(self._ipaddr, timeout=5)` gets instantiated, with the parameters received from config or parameters obtained after network scanning. Notice `WifiLedBulb` class is from the installed `flux-led` component, which is used to do all the communication with the RGB ontroller. Now we are ready to play with the LED strip:

```python
class FluxLight(Light):
...
    def turn_on(self, **kwargs):
        """Turn the specified or all lights on."""
        if not self.is_on:
            self._bulb.turnOn()

        hs_color = kwargs.get(ATTR_HS_COLOR)

        if hs_color:
            rgb = color_util.color_hs_to_RGB(*hs_color)
        else:
            rgb = None

        brightness = kwargs.get(ATTR_BRIGHTNESS)
        effect = kwargs.get(ATTR_EFFECT)
        white = kwargs.get(ATTR_WHITE_VALUE)

        # Show warning if effect set with rgb, brightness, or white level
        if effect and (brightness or white or rgb):
            _LOGGER.warning(
                "RGB, brightness and white level are ignored when"
                " an effect is specified for a flux bulb"
            )

        # Random color effect
        if effect == EFFECT_RANDOM:
            self._bulb.setRgb(
                random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
            )
            return

        if effect == EFFECT_CUSTOM:
            if self._custom_effect:
                self._bulb.setCustomPattern(
                    self._custom_effect[CONF_COLORS],
                    self._custom_effect[CONF_SPEED_PCT],
                    self._custom_effect[CONF_TRANSITION],
                )
            return

        # Effect selection
        if effect in EFFECT_MAP:
            self._bulb.setPresetPattern(EFFECT_MAP[effect], 50)
            return

        # Preserve current brightness on color/white level change
        if brightness is None:
            brightness = self.brightness

        # Preserve color on brightness/white level change
        if rgb is None:
            rgb = self._bulb.getRgb()

        if white is None and self._mode == MODE_RGBW:
            white = self.white_value

        # handle W only mode (use brightness instead of white value)
        if self._mode == MODE_WHITE:
            self._bulb.setRgbw(0, 0, 0, w=brightness)

        # handle RGBW mode
        elif self._mode == MODE_RGBW:
            self._bulb.setRgbw(*tuple(rgb), w=white, brightness=brightness)

        # handle RGB mode
        else:
            self._bulb.setRgb(*tuple(rgb), brightness=brightness)
```

1. When flipping the switch from the graphical interface, `turn_on(self, **kwargs)` gets called without any arguments which than calls `self._bulb.turnOn()`, and than updates parameters like `brightness`, `rgb`, `white`.

2. If selecting a color in the color pallete, `turn_on(self, **kwargs)` gets called with arguments, for example: `{'hs_color': (33.06441793768991, 62.317572791621124)}`, which gets converted to `rgb` `(255, 183, 96)`, than `self._bulb.setRgbw(*tuple(rgb), w=white, brightness=brightness)` is called to set the collor.

3. If changing the `Brightness` in the UI, `turn_on(self, **kwargs)` gets called with arguments, for example: `{'brightness': 91}`, than `self._bulb.setRgbw(*tuple(rgb), w=white, brightness=brightness)` gets called to set the brightness.

4. If changing the `White value` in the UI, same as above happens but with different arguments: `{'white_value': 19}`.

5. Changing effect is also possible, there are effects like all kinds of color strobes, or color fades, or colorjump. For example selecting the `collorloop` effect in the dropdown list `turn_on(self, **kwargs)` gets called with arguments `{'effect': 'colorloop'}`, than `self._bulb.setPresetPattern(EFFECT_MAP[effect], 50)` to set the `collorloop` effect. Notice the `EFFECT_MAP[effect]` which maps to some magic number `EFFECT_COLORLOOP: 0x25,` that for RGB LED strip controller means `collorloop` effect. Now you should see your RGB light strip slightly changing collors.


![](https://raw.githubusercontent.com/adrianalin/home-assistant_series/master/wifi_rgb_led_strip/controls.png)


There is also possibility to select a random effect from the available ones. Also may be possible to set a customized effect (still need to look into that).

Than there are simple methods like turn off:

```python
    def turn_off(self, **kwargs):
        """Turn the specified or all lights off."""
        self._bulb.turnOff()
```

And `@property` methods to access status, like `def available(self) -> bool:` , `def name(self):`, `def is_on(self):`, `def brightness(self):`, `def hs_color(self):`, `def supported_features(self):`, `def white_value(self):`, `def effect_list(self):` that are quite small methods and self explanatory.

Here is the github repo of a minimal working class https://github.com/adrianalin/home-assistant_series/tree/master/wifi_rgb_led_strip