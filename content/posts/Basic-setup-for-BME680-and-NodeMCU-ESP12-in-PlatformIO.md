---
title: "Basic Setup for BME680 and NodeMCU ESP12 in PlatformIO"
date: 2019-06-06T21:32:31+03:00
draft: false
tags: [ "nodemcu" ]
---
In this post I describe how to quickly get going with reading temperature, air pressure, air quality and other parameters from the Bosch BME680 with PlatformIO using Arduino framework. I followed the instructions on the github page of bsec, but could not get it working so I decided to write this blog post.


![](https://raw.githubusercontent.com/adrianalin/NodeMCU_workspace/master/pictures/pic_1_Basic_setup_for_BME680_and_NodeMCU_ESP12E_in_PlatformIO.jpg)


This is the sensor that I used here.


![](https://raw.githubusercontent.com/adrianalin/NodeMCU_workspace/master/pictures/pic_2_Basic_setup_for_BME680_and_NodeMCU_(ESP12E)_in_PlatformIO.jpg)


This is my NodeMCU (ESP12E)


### Getting the library

First you should download the library as zip from https://github.com/BoschSensortec/BSEC-Arduino-library rename it to `bsec` and put it in your `~/Arduino/libraries` folder:

![](https://raw.githubusercontent.com/adrianalin/NodeMCU_workspace/master/pictures/pic_3_Basic_setup_for_BME680_and%20NodeMCU_(ESP12E)%20in%20PlatformIO.jpg)

### Creating the project in PlatformIO

Now you can create a new project in PlatformIO by clicking New Project, Name: basic_bme680, Board: NodeMCU 1.0 (ESP-12E Module), Framework: Arduino.

You can now copy/paste the content from `~/Arduino/libraries/bsec/examples/basic/basic.ino` into newly created project's main.cpp. I needed to change the `BME680_I2C_ADDR_PRIMARY` to `BME680_I2C_ADDR_SECONDARY`. Add the bsec library to project platform.in file by appending these two line:

````bash
lib_extra_dirs = /home/youruser/Arduino/libraries
build_flags = -L/home/youruser/Arduino/libraries/bsec/src/esp8266 -lalgobsec
````

### ESP8266 - modify the linker script header

As mentioned on https://github.com/BoschSensortec/BSEC-Arduino-library: Due to the architecture of the ESP8266's memories and current size of the BSEC library, upon compilation, you will receive an error:
`section .text1 will not fit in region iram1_0_seg`
In order to solve this, you will need to modify the linker script and specifically define where the library should be placed in memory.
Linker script is located `~/.arduino15/packages/esp8266/hardware/esp8266/2.5.0-beta3/tools/sdk/ld/eagle.app.v6.common.ld.h`
After

````bash
*libwpa.a:(.literal.* .text.*)
*libwpa2.a:(.literal.* .text.*)
*libwps.a:(.literal.* .text.*)
````

Add

````bash
*libalgobsec.a:(.literal.* .text.*)
````

### Wiring BME680 to NodeMCU (ESP12E)

ESP12E-----------BME680
GND------------>GND
3.3V------------>3.3V
D2------------>SDA
D1------------>SCL


### Results

After building and running the application on NodeMCU, this is the output you should get if you go to PlatformIO->Serial Monitor.

````bash
adrian@adrian-UX310UQK:~/esp8266/basic_bme680$ pio device monitor --port /dev/ttyUSB0
--- Miniterm on /dev/ttyUSB0  115200,8,N,1 ---
--- Quit: Ctrl+C | Menu: Ctrl+T | Help: Ctrl+T followed by Ctrl+H ---


BSEC library version 1.4.7.3
Timestamp [ms], raw temperature [°C], pressure [hPa], raw relative humidity [%], gas [Ohm], IAQ, IAQ accuracy, temperature [°C], relative humidity [%], Static IAQ, CO2 equivalent, breath VOC
 equivalent
34212, 25.98, 97248.38, 55.96, 97294.56, 25.00, 0, 25.98, 55.96, 25.00, 500.00, 0.50
37212, 26.02, 97248.35, 55.86, 92149.54, 25.00, 0, 25.96, 55.97, 25.00, 500.00, 0.50
40212, 26.06, 97247.36, 55.64, 91051.63, 25.00, 0, 25.99, 55.68, 25.00, 500.00, 0.50
43212, 26.07, 97249.90, 55.39, 91647.23, 25.00, 0, 26.01, 55.43, 25.00, 500.00, 0.50
46212, 26.07, 97246.62, 55.11, 92657.40, 25.00, 0, 26.01, 55.18, 25.00, 500.00, 0.50
````


BME680 is a metal oxide-based sensor that detects VOCs by adsorption (and subsequent oxidation/reduction) on its sensitive layer. Thus, BME680 reacts to most volatile compounds polluting indoor air (one exception is for instance CO2). In contrast to sensors selective for one specific component, BME680 is capable of measuring the sum of VOCs/contaminants in the surrounding air. This enables BME680 to detect e.g. outgassing from paint, furniture and/or garbage, high VOC levels due to cooking, food consumption, exhaled breath and/or sweating.

As a raw signal, BME680 will output resistance values and its changes due to varying VOC concentrations (the higher the concentration of reducing VOCs, the lower the resistance and vice versa). Since this raw signal is influenced by parameters other than VOC concentration (e.g. humidity level), the raw values are transformed to an indoor air quality (IAQ) index by smart algorithms inside BSEC.

The IAQ scale ranges from 0 (clean air) to 500 (heavily polluted air). During operation, the algorithms automatically calibrate and adapt themselves to the typical environments where the sensor is operated (e.g., home, workplace, inside a car, etc.). This automatic background calibration ensures that users experience consistent IAQ performance. The calibration process considers the recent measurement history (typ. up to four days) to ensure that IAQ ~ 25 corresponds to “typical good” air and IAQ ~ 250 indicates “typical polluted” air. IAQ accuracy indicates the confidence level of the algorithm's IAQ output. 0 standing for no confidence and 3 standing for the highest.

For more info on the sensor, see https://cdn-shop.adafruit.com/product-files/3660/BME680.pdf

Here is the project: https://github.com/adrianalin/NodeMCU_workspace/tree/master/basic_bme680
