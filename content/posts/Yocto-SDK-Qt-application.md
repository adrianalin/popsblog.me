---
title: "Yocto SDK Qt Application"
date: 2020-08-29T12:36:46+03:00
draft: false
tags: [ "raspberrypi", "linux", "yocto" ]
---

Lately I explored the Yocto SDK and devtool.

#### Yocto SDK

The idea behind the SDK is to be installed on any machine and to be used for application development, kernel development, etc. The machine that has the SDK installed does not have to be associated with the machine that has the Yocto Project installed.

In order to build the SDK, on the machine with the Yocto project installed:
- source the `oe-init-build-env`
- run the following command; (I used my custom Yocto layer for Raspberry Pi here https://github.com/adrianalin/meta-rpi)
```bash
$ bitbake -c do_populate_sdk console-basic-image
``` 
- after finishing you should find the generted `poky-glibc-x86_64-console-basic-image-cortexa7t2hf-neon-vfpv4-raspberrypi3-toolchain-3.1.2.sh` in `build/tmp/deploy/sdk`. This is a self contained shell script, that you distribute to other application developers (that don't have the Yocto project installed on their machines). They just need to execute the script in order to install the SDK in `/opt/poky/3.1.2` - the version may differ.

If you look in this script, you see that at the end is a long binary string marked with `MARKER:` that is the payload. The payload is dumped to a `sdk.zip`, than extracted to `$target_sdk_dir`, and than removed. 


If you look where the SDK was installed (/opt/poky/3.1.2) you will find:
- `environment-setup-cortexa7t2hf-neon-vfpv4-poky-linux-gnueabi` this is a shell script you `source` to have the environment setup like: `SDKTARGETSYSROOT`, `OECORE_NATIVE_SYSROOT`, append compilers to `PATH`, `CC`, `CXX`, `CPP` and so on.

#### `devtool` for modifying the source of existing recipe

`userland` library is brought in Yocto with the official `meta-raspberrypi` layer (git://git.yoctoproject.org/meta-raspberrypi). Since I was curious about `userland` project from Raspberry Pi, I was thinking about how can I change the sources, poke around, and easily redeploy the project. I found `devtool` to do exactly what I needed:

- `devtool modify userland` to modify the source for an existing recipe. This command would create a directory `build/workspace/sources/userland` this is the userland repository which you have now cloned localy and can modify it.
- `devtool build userland` to build the project
- `devtool deploy-target userland root@192.168.0.94` to deploy the newly built libraries to Raspberry PI

But I wanted more, like use an IDE, remote debugging and stepping through code, debugging the libraries, and so on. Since this is a cmake project, QtCreator can be used to do all these.

#### Setting up QtCreator

In QtCreator I added a new kit called RPI, setup the paths to the compilers and debugger:
* `/opt/poky/3.1.2/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-g++`
* `/opt/poky/3.1.2/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-gcc`
* `/opt/poky/3.1.2/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-gdb`)
These flags are from `environment-setup-cortexa7t2hf-neon-vfpv4-poky-linux-gnueabi`.

If you look into `build/workspace/sources/userland` you will find `makefiles/cmake/toolchains/arm-linux-gnueabihf.cmake` cmake toolchain file. Adjust the compiler to use `arm-poky-linux-gnueabi-*`.
Also set the following compile flags:
```bash
SET (CMAKE_C_FLAGS " -mthumb -mfpu=neon-vfpv4 -mfloat-abi=hard -mcpu=cortex-a7 -fstack-protector-strong -D_FORTIFY_SOURCE=2 -Wformat -Wformat-security -Werror=format-security --sysroot=/opt/poky/3.1.2/sysroots/cortexa7t2hf-neon-vfpv4-poky-linux-gnueabi")
+SET (CMAKE_CXX_FLAGS " -mthumb -mfpu=neon-vfpv4 -mfloat-abi=hard -mcpu=cortex-a7 -fstack-protector-strong -D_FORTIFY_SOURCE=2 -Wformat -Wformat-security -Werror=format-security --sysroot=/opt/poky/3.1.2/sysroots/cortexa7t2hf-neon-vfpv4-poky-linux-gnueabi ")
```

Now launch QtCreator, and look for `userland/CMakeLists.txt` and open it, specify the `RPI` kit you just added.
You may get an error like `The C Compiler is not able to compile a simple test program`. This happens because cmake is trying to compile and run a simple test program. But since we are cross-compiling this is not possible. In order to disable this, add `SET (CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY")` so that cmake tries to compile a static library instead of executable.

In QtCreator is also needed to set the `CMAKE_TOOLCHAIN_FILE` to `build/workspace/sources/userland/makefiles/cmake/toolchains/arm-linux-gnueabihf.cmake` for the cross-compile setup (Projects->RPI->Build->CMAKE, and add a new variable).

Now the cmake project should open and compile successfully. In order to deply to target, go to Tools->Options->Devices and add a Generic Linux Device, and fill in the Raspberry Pi details (like IP address, ssh keys...).

You can now run the project, deployment step should take place, and when running you may get an error saying: `error while loadin shared libraries ...` , you also need to set `LD_LIBRARY_PATH` to `/opt/vc/lib` (Projects->Run->Environment and add new environment varible). Deploy and run should now work.

For debugging, make sure `gdbserver` is available on Raspberry Pi. Also the RPI kit should have the debugger set to `/opt/poky/3.1.2/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-gdb`. If you get a warning something like`Do you need "set solib-search-path" or "set sysroot"?` you also need to add `set solib-search-path /tmp/QtCreator-GkclXP/opt/vc/lib/` (temporary deploy path) in Project->Debugger Settings->Additional startup commands. This allows you to step through shared library code.
