---
title: "Show Real Time Data From NodeMCU With Flask and SocketIO"
date: 2019-08-31T18:55:02+03:00
draft: false
tags: [ "python", "nodemcu", "micropython" ]
---

In this post I describe how to use Flask to show real time measurement data from NodeMCU and BME680 using SocketIO. NodeMCU reads data from BME680 sensor, and sends the data to Flask server to be displayed in browser.

![](https://raw.githubusercontent.com/adrianalin/bme680_nodemcu_socketio/master/screenshot.gif)

This application consists of multiple components:
- __NodeMCU client__: runs MicroPython, reads air tempeature, pressure, humidity from BME680 and sends the readings via SocketIO to Flask server
- __Browser client__: connects to Flask server, and receives the real time measurement data to be plotted in browser using Chart.js and Google charts
- __Flask server__: uses Flask-SocketIO for receiving data from NodeMCU and sends data to conected browser clients


### SocketIO basics

SocketIO gives applications access to low latency bi-directional communications between the clients and the server. It provides failovers to other protocols if WebSockets are not available on the browser or the server.
Socket.IO simplifies the WebSocket API and unifies the APIs of its fallback transports. Transports include:
- WebSocket
- Flash Socket
- AJAX long-polling
- AJAX multipart streaming
- IFrame
- JSONP polling

When using SocketIO, messages are received by both parties as events. On the client side Javascript callbacks are used. With Flask-SocketIO the server needs to register handlers for these events, similarly to how routes are handled by view functions.
SocketIO servers and clients can push each other messages at any time, it does not require a request from the client.


### NodeMCU client

Reads temperature, humidity and pressure from BME680 sensor and sends it to flask server via SocketIO.
Data readings from the sensor is done with BME680 library <a href="https://github.com/pimoroni/bme680-python">https://github.com/pimoroni/bme680-python</a> with small changes in order to work with NodeMCU. Note: Each of the `*.py` file needs to be compiled  to `.mpy` in order to avoid memory errors (using <a href="https://github.com/micropython/micropython/tree/master/mpy-cross">https://github.com/micropython/micropython/tree/master/mpy-cross</a>). Here is how to setup a connection via I2C to BME680 and read data.

```python
# construct an I2C bus
i2c_dev = I2CAdapter(scl=Pin(5), sda=Pin(4), freq=100000)
sensor = bme680.BME680(bme680.I2C_ADDR_SECONDARY, i2c_device=i2c_dev)

# These oversampling settings can be tweaked to
# change the balance between accuracy and noise in
# the data.
sensor.set_humidity_oversample(bme680.OS_2X)
sensor.set_pressure_oversample(bme680.OS_4X)
sensor.set_temperature_oversample(bme680.OS_8X)
sensor.set_filter(bme680.FILTER_SIZE_3)


def get_sensor_data():
    output = ""
    if sensor.get_sensor_data():
        output = "{}, {}, {}, {}".format(
            sensor.data.temperature,
            sensor.data.pressure,
            sensor.data.humidity,
            sensor.data.gas_resistance)
    return output
```

Data sending to Flask is done with <a href="https://github.com/danni/uwebsockets">https://github.com/danni/uwebsockets</a>, also i did some changes here (like removing the logging module dependencies to reduce size). Notice that we are sending messages to server, using the `bme680_data` event, on the server side we have a handler for this event. Connecting to Flask server, and send the sensor measurements:
```python
with client.connect('http://yourIP:5000/') as socketio:

    @socketio.at_interval(1)
    def send_measurement():
        gc.collect()
        data = get_sensor_data()
        try:
            socketio.emit("bme680_data", data)
        except Exception as e:
            print("Exception happened: ", e)


    socketio.run_forever()
```


### Flask server

For the Flask server I used Flask-SocketIO. The server looks like this:
```python
clients = []

@app.route('/')
def index():
    """
    Serve a JS page that upgrades you to a websocket.
    """
    return render_template('index.html')

@socketio.on('connect')
def on_connect():
    clients.append(request.sid)

@socketio.on('disconnect')
def on_disconnect():
    clients.remove(request.sid)

@socketio.on('bme680_data')
def handle_bme680_data(data):
    temperature, pressure, humidity, gas = data.split(',')
    dict_data = {
        'timestamp': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'temperature': float(temperature),
        'pressure': float(pressure),
        'humidity': float(humidity),
        'gas': float(gas),
    }
    for client in clients:
        if client != request.sid:
            socketio.emit('js_client', json.dumps(dict_data), room=client)

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', debug=True)
```
`clients` is a list of connected clients that receive the data. The `handle_bme680_data` handler for `bme680_data` event, receives data from NodeMCU, and at each data receive it sends it to all connected `js_client` items that represent actual browser clients that receive and plot the data.


### Browser client

Browser clients receive data from server to be plotted in browser. In Javascript there is a function that handles `js_client` event and plots the data. There are lots of options in order to customize the ChartJS and Google chart gauges that make most of this code (not shown here).
```bash
var socket = io();
google.charts.load('current', {'packages':['gauge']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    ...
    socket.on('js_client', function(data) {
    	const json_data = JSON.parse(data);
    }
    ...
}
```

Finally here is the entire github project both NodeMCU code and the Flask server: <a href="https://github.com/adrianalin/bme680_nodemcu_socketio">https://github.com/adrianalin/bme680_nodemcu_socketio</a>.