---
title: "Linux Boot Options on STM32F769I DISCO"
date: 2020-04-13T10:06:45+03:00
draft: false
tags: [ "linux", "buildroot", "stm32" ]
---

Preboot eXecution Environment is a standardized client-server environment that boots a software assembly, retrieved from a network. This allows you to have multiple boot configurations (must be available on tftp), and select at boot time configuration you want.

#### Setup

* `u-boot-spl.bin` written on SPI NOR Flash memory at `0x08000000` (sector 0 of Flash memory on AXIM interface), controlled by Boot pin, BOOT_ADD0 and BOOT_ADD1.
* `u-boot.bin` also on NOR Flash at address `0x08008000`

After U-Boot starts, make sure the following variables are set to fit your setup:
```bash
U-Boot > setenv ipaddr xx.xx.xx.xx
U-Boot > setenv serverip xx.xx.xx.xx
```

#### Boot with initramfs

The initial RAM filesystem is a ramfs which is loaded by the bootloader and that is mounted as root before the normal boot procedure. It is typically used to load modules needed to mount the "real" root file system, etc.
`run netboot` is the command I added to U-Boot as default, to start Linux. This expects a `zImage` with initramfs (`CONFIG_BLK_DEV_INITRD=y`), and the device tree, on tftp server.

#### Boot with rootfs on the SD Card

* In buildroot disable `BR2_TARGET_ROOTFS_INITRAMFS`.
* Modify the Linux kernel configuration to not include the rootfs into the uImage/zImage. Unset `CONFIG_BLK_DEV_INITRD` since we are going to use sdcard mounted filesystem.
If using uImage make sure to also fill in `BR2_LINUX_KERNEL_UIMAGE_LOADADDR` which is `0xC0008000` (which is the location where booloader loads the uImage) in my case.

* Copy the uImage/zImage to tftp server. Device tree should also be available on tftp server.

* Make sure SD card support is enabled in kernel menuconfig `Device Drivers -> MMC/SD/SDIO card support`, and `CONFIG_MMC_STM32_SDMMC`.

I had an sd card used for Raspberry Pi, and I just rewrote the second ext4 partition:
```bash
sudo dd if=output/images/rootfs.ext2 of=/dev/sdb2
```

More info: https://www.emcraft.com/som/vf6/booting-rootfs-on-sd

#### Boot using `pxe` U-Boot command

On tftp server i added a new directory `pxelinux.cfg` with a new file `C0A8007B` name set to hexadecimal IP of my board.

```bash
$ tree /srv/tftp/
/srv/tftp/
├── pxelinux.cfg
│   └── C0A8007B
└── stm32f769
    ├── stm32f769-disco.dtb
    ├── uImage
    ├── zImage
```

`C0A8007B` looks like:

```bash
menu title Select the boot mode
timeout 10

DEFAULT sdcard

initramfs
LABEL initramfs
        KERNEL stm32f769/zImage
        FDT stm32f769/stm32f769-disco.dtb

sdcard
LABEL sdcard
        KERNEL stm32f769/zImage
        FDT stm32f769/stm32f769-disco.dtb
        APPEND root=/dev/mmcblk0p2 rootwait rw
```

First option is for `initramfs` image with `zImage` and device tree available on tftp server.
Second option is for `zImage` on tftp with rootfs on sd card, notice how kernel boot parameters are set with `APPEND`.

When starting u-boot, press `space` key to stop autoboot and prepare for pxe boot:
* Get the config file from tftp server.
* Than interpret the pxe file stored in memory.

More info: https://github.com/lentinj/u-boot/blob/master/doc/README.pxe

```bash
U-Boot > pxe get
...
Retrieving file: pxelinux.cfg/C0A8007B
Filename 'pxelinux.cfg/C0A8007B'.
Load address: 0xc0008000
...
U-Boot > pxe boot
Select the boot mode
1:      initramfs
2:      sdcard
Enter choice: 2:        sdcard
```

It's possible to add `CONFIG_BOOTCOMMAND="pxe get && pxe boot"` to uboot menuconfig to automatically get and boot with pxe.

The boot log should lok like:
```bash
...
mmci-pl18x 40011c00.sdio2: Got CD GPIO
mmci-pl18x 40011c00.sdio2: mmc0: PL180 manf 80 rev8 at 0x40011c00 irq 39,0 (pio)
sdhci: Secure Digital Host Controller Interface driver
sdhci: Copyright(c) Pierre Ossman
sdhci-pltfm: SDHCI platform and OF driver helper
mmc0: host does not support reading read-only switch, assuming write-enable
mmc0: new SDHC card at address 0002
mmcblk0: mmc0:0002 00000 3.70 GiB
 mmcblk0: p1 p2
...
EXT4-fs (mmcblk0p2): recovery complete
EXT4-fs (mmcblk0p2): mounted filesystem with ordered data mode. Opts: (null)
VFS: Mounted root (ext4 filesystem) on device 179:2.
...
```

Notice rootfs mounted on ext4:
```bash
~ # mount
/dev/root on / type ext4 (rw,relatime)
devtmpfs on /dev type devtmpfs (rw,relatime)
proc on /proc type proc (rw,relatime)
none on /dev/shm type tmpfs (rw,relatime,mode=777)
none on /tmp type tmpfs (rw,relatime,mode=1777)
none on /run type tmpfs (rw,nosuid,nodev,relatime)
sysfs on /sys type sysfs (rw,relatime)
```

#### XIP boot

Buildroot config:

https://www.digi.com/resources/documentation/digidocs/PDFs/90000852.pdf

```bash

U-Boot > sf probe
SF: Detected mx66l51235l with page size 256 Bytes, erase size 64 KiB, total 64 MiB
U-Boot > tftp ${kernel_addr_r} stm32f769/uImage
done
U-Boot > tftp ${fdt_addr_r} ${dtb_file}
Bytes transferred = 2935760 (2ccbd0 hex)
U-Boot > sf erase 0 +2ccbd0
U-Boot > sf write ${kernel_addr_r} 0 2ccbd0
U-Boot > bootm ${kernel_addr_r} - ${fdt_addr_r}

```

```
U-Boot > sf probe
U-Boot > tftp ${kernel_addr_r} stm32f769/xipImage
U-Boot > sf write ${kernel_addr_r} 0 0x2ccbd0
U-Boot > cmp.b ${kernel_addr_r} 0x90000000 0x2ccbd0
U-Boot > md.b 0x90000000 10

U-Boot > tftp ${fdt_addr_r} ${dtb_file}
U-Boot > bootx ${kernel_addr_r} ${fdt_addr_r}
```

Kernel config:
```bash
CONFIG_BLK_DEV_INITRD=y
CONFIG_XIP_KERNEL=y
```

U-boot config:
```bash

```

#### Resources

https://www.emcraft.com/stm32f7-discovery-board/using-sd-card-in-linux
[Github repo for STM32F769I-Disco](https://github.com/adrianalin/STM32F769I-disco_Buildroot)
