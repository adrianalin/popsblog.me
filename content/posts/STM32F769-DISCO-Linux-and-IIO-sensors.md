---
title: "STM32F769-DISCO Linux and IIO sensors"
date: 2020-05-21T22:18:41+03:00
draft: false
tags: [ "linux", "buildroot" , "stm32"]
---

The Industrial I/O subsystem is intended to provide support for devices (I2C or SPI sensors) that in some sense are analog to digital or digital to analog converters (ADCs, DACs) (see https://wiki.analog.com/software/linux/docs/iio/iio for more details).
I will decribe how to configure and make use of IIO to interface with MPU6050 acelerometer.


#### `libiio` and `iiod` daemon

Since I am using buildroot to build the entire system (bootloader, linux, rootfs, busybox, packages), it's simple to enable `libiio` in buildroot menuconfig. The only problem is that it's slightly outdated (version 0.18), and I updated mine to 0.19. Seems that libiio was updated in https://github.com/buildroot/buildroot/commit/c88e911609198d5a65cf832269fb4ed41760f455.

On the target you run the daemon `iiod`. Buildroot package provides a startup script `S99iiod` for busybox init, which should start the daemon automatically.

There are multiple backends available for IIO interfacing: local backend, XML backend, network backend (which I will show how to use), USB backend, serial backend.

The network backend acts as a client-server application. `iiod` running on the target acts as server, while the client (running on a laptop on same network) uses `libiio` api to find and communicate with the server. There are a few examples here https://github.com/analogdevicesinc/libiio/tree/master/examples.

From the client, first you create a context `iio_create_context_from_uri('ip:192.168.0.121')`, which returns a `struct iio_context*` which can be printed as xml:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE context [
  <!ELEMENT context (device|context-attribute)*>
  <!ELEMENT context-attribute EMPTY>
  <!ELEMENT device (channel|attribute|debug-attribute|buffer-attribute)*>
  <!ELEMENT channel (scan-element?,attribute*)>
  <!ELEMENT attribute EMPTY>
  <!ELEMENT scan-element EMPTY>
  <!ELEMENT debug-attribute EMPTY>
  <!ELEMENT buffer-attribute EMPTY>
  <!ATTLIST context name CDATA #REQUIRED>
  <!ATTLIST context description CDATA #IMPLIED>
  <!ATTLIST context-attribute name CDATA #REQUIRED>
  <!ATTLIST context-attribute value CDATA #REQUIRED>
  <!ATTLIST device id CDATA #REQUIRED>
  <!ATTLIST device name CDATA #IMPLIED>
  <!ATTLIST channel id CDATA #REQUIRED>
  <!ATTLIST channel type (input|output) #REQUIRED>
  <!ATTLIST channel name CDATA #IMPLIED>
  <!ATTLIST scan-element index CDATA #REQUIRED>
  <!ATTLIST scan-element format CDATA #REQUIRED>
  <!ATTLIST scan-element scale CDATA #IMPLIED>
  <!ATTLIST attribute name CDATA #REQUIRED>
  <!ATTLIST attribute filename CDATA #IMPLIED>
  <!ATTLIST debug-attribute name CDATA #REQUIRED>
  <!ATTLIST buffer-attribute name CDATA #REQUIRED>
]>
<context name="xml" description="Linux buildroot 5.6.0-next-20200412 #22 PREEMPT Thu May 21 16:10:52 EEST 2020 armv7ml">
   <context-attribute name="local,kernel" value="5.6.0-next-20200412" />
   <device id="iio:device0" name="mpu6050">
      <channel id="accel_x" type="input">
         <scan-element index="0" format="be:S16/16&gt;&gt;0" scale="0.000598" />
         <attribute name="calibbias" filename="in_accel_x_calibbias" />
         <attribute name="matrix" filename="in_accel_matrix" />
         <attribute name="mount_matrix" filename="in_accel_mount_matrix" />
         <attribute name="raw" filename="in_accel_x_raw" />
         <attribute name="scale" filename="in_accel_scale" />
         <attribute name="scale_available" filename="in_accel_scale_available" />
      </channel>
      <channel id="accel_y" type="input">
         <scan-element index="1" format="be:S16/16&gt;&gt;0" scale="0.000598" />
         <attribute name="calibbias" filename="in_accel_y_calibbias" />
         <attribute name="matrix" filename="in_accel_matrix" />
         <attribute name="mount_matrix" filename="in_accel_mount_matrix" />
         <attribute name="raw" filename="in_accel_y_raw" />
         <attribute name="scale" filename="in_accel_scale" />
         <attribute name="scale_available" filename="in_accel_scale_available" />
      </channel>
      <channel id="accel_z" type="input">
         <scan-element index="2" format="be:S16/16&gt;&gt;0" scale="0.000598" />
         <attribute name="calibbias" filename="in_accel_z_calibbias" />
         <attribute name="matrix" filename="in_accel_matrix" />
         <attribute name="mount_matrix" filename="in_accel_mount_matrix" />
         <attribute name="raw" filename="in_accel_z_raw" />
         <attribute name="scale" filename="in_accel_scale" />
         <attribute name="scale_available" filename="in_accel_scale_available" />
      </channel>
      <channel id="anglvel_x" type="input">
         <scan-element index="4" format="be:S16/16&gt;&gt;0" scale="0.001065" />
         <attribute name="calibbias" filename="in_anglvel_x_calibbias" />
         <attribute name="mount_matrix" filename="in_anglvel_mount_matrix" />
         <attribute name="raw" filename="in_anglvel_x_raw" />
         <attribute name="scale" filename="in_anglvel_scale" />
         <attribute name="scale_available" filename="in_anglvel_scale_available" />
      </channel>
      <channel id="anglvel_y" type="input">
         <scan-element index="5" format="be:S16/16&gt;&gt;0" scale="0.001065" />
         <attribute name="calibbias" filename="in_anglvel_y_calibbias" />
         <attribute name="mount_matrix" filename="in_anglvel_mount_matrix" />
         <attribute name="raw" filename="in_anglvel_y_raw" />
         <attribute name="scale" filename="in_anglvel_scale" />
         <attribute name="scale_available" filename="in_anglvel_scale_available" />
      </channel>
      <channel id="anglvel_z" type="input">
         <scan-element index="6" format="be:S16/16&gt;&gt;0" scale="0.001065" />
         <attribute name="calibbias" filename="in_anglvel_z_calibbias" />
         <attribute name="mount_matrix" filename="in_anglvel_mount_matrix" />
         <attribute name="raw" filename="in_anglvel_z_raw" />
         <attribute name="scale" filename="in_anglvel_scale" />
         <attribute name="scale_available" filename="in_anglvel_scale_available" />
      </channel>
      <channel id="timestamp" type="input">
         <scan-element index="7" format="le:S64/64&gt;&gt;0" />
      </channel>
      <channel id="temp" type="input">
         <attribute name="offset" filename="in_temp_offset" />
         <attribute name="raw" filename="in_temp_raw" />
         <attribute name="scale" filename="in_temp_scale" />
      </channel>
      <channel id="gyro" type="input">
         <attribute name="matrix" filename="in_gyro_matrix" />
      </channel>
      <attribute name="current_timestamp_clock" />
      <attribute name="sampling_frequency" />
      <attribute name="sampling_frequency_available" />
      <buffer-attribute name="data_available" />
      <buffer-attribute name="watermark" />
   </device>
   <device id="trigger0" name="mpu6050-dev0" />
</context>
```

Than from context use:
* `device = iio_context_get_device(context, 0)` to get the `mpu6050`
* `channel = iio_device_get_channel(device, 0)` to get channel 0 `accel_x`
* `trigger = iio_context_get_device(context, 1)` to get the `trigger0`
* `iio_device_set_trigger(device, trigger)` associate a trigger to a given device
* `iio_channel_enable(channel)` before creating a iio_buffer need to enable a channel
* `iio_device_create_buffer(device, SAMPLES, 0)` create buffer associated to the given device

#### IIO Interactive mode

`iiod -id` starts iiod in interactive mode. Looking at `parser.y` you find the supported commands or type `HELP` at the prompt:

```bash
~# ./iiod -id
iio-daemon > HELP
Available commands:

        HELP
                Print this help message
        EXIT
                Close the current session
        PRINT
                Displays a XML string corresponding to the current IIO context
        VERSION
                Get the version of libiio in use
        TIMEOUT <timeout_ms>
                Set the timeout (in ms) for I/O operations
        OPEN <device> <samples_count> <mask> [CYCLIC]
                Open the specified device with the given mask of channels
        CLOSE <device>
                Close the specified device
        READ <device> DEBUG|BUFFER|[INPUT|OUTPUT <channel>] [<attribute>]
                Read the value of an attribute
        WRITE <device> DEBUG|BUFFER|[INPUT|OUTPUT <channel>] [<attribute>] <bytes_count>
                Set the value of an attribute
        READBUF <device> <bytes_count>
                Read raw data from the specified device
        WRITEBUF <device> <bytes_count>
                Write raw data to the specified device
        GETTRIG <device>
                Get the name of the trigger used by the specified device
        SETTRIG <device> [<trigger>]
                Set the trigger to use for the specified device
        SET <device> BUFFERS_COUNT <count>
                Set the number of kernel buffers for the specified device
iio-daemon > OPEN iio:device0 50 00000001
DEBUG: Mask[0]: 0x00000001
DEBUG: Added thread to client list
DEBUG: Adding new device thread to device list
DEBUG: R/W thread started for device mpu6050
WARNING: High-speed mode not enabled
DEBUG: IIO device iio:device0 reopened with new mask:
DEBUG: Mask[0] = 0x00000001
0
iio-daemon > READBUF iio:device0 10
DEBUG: Waiting for completion...
ERROR: Reading from device failed: -110
DEBUG: Stopping R/W thread for device mpu6050
DEBUG: Exiting rw_buffer with code -110
ERROR: Connection timed out
```

#### I2C IIO MPU6050

Hardware connection:

```bash
STM32F769i-disco (arduino connector)		MPU6050
3V3-----------------------------------------3V3
3V3-----------------------------------------VIO
GND-----------------------------------------GND
D8------------------------------------------INT
D15-----------------------------------------SCL
D14-----------------------------------------SDA
```

There is already a driver available in the kernel for this accelerometer, just needs to be configured in devicetree and enable in menuconfig, and enable the IIO interface:

* Enable `CONFIG_IIO_INTERRUPT_TRIGGER`, `CONFIG_INV_MPU6050_IIO=y` and `CONFIG_INV_MPU6050_I2C=y` in `make linux-menuconfig`.
* Enable `BR2_PACKAGE_LIBIIO=y`, `BR2_PACKAGE_LIBIIO_NETWORK_BACKEND=y`, `BR2_PACKAGE_LIBIIO_IIOD=y` to enable `iiod`.
* Enable `BR2_PACKAGE_LINUX_TOOLS=y` and `BR2_PACKAGE_LINUX_TOOLS_IIO=y` in order to access `iio_generic_buffer` in buildroot menuconfig.
* Add MPU6050 support in devicetree `&i2c1` existing section:

```bash
&i2c1 {
	....
	mpu6050: accelerometer@68 {
		compatible = "invensense,mpu6050";
		reg = <0x68>;
		interrupt-parent = <&gpioj>;
		interrupts = <4 1>;
		mount-matrix = "-0.984807753012208",  /* x0 */
		               "0",                   /* y0 */
		               "-0.173648177666930",  /* z0 */
		               "0",                   /* x1 */
		               "-1",                  /* y1 */
		               "0",                   /* z1 */
		               "-0.173648177666930",  /* x2 */
		               "0",                   /* y2 */
		               "0.984807753012208";   /* z2 */
	};
};
```

Now boot linux and run:

```bash
~ # lsiio
Device 000: mpu6050
Trigger 000: mpu6050-dev0

~ # iio_generic_buffer --device-name mpu6050 -a
iio device number being used is 0
iio trigger number being used is 0
Enabling all channels
Enabling: in_accel_y_en
Enabling: in_anglvel_z_en
Enabling: in_accel_x_en
Enabling: in_timestamp_en
Enabling: in_anglvel_y_en
Enabling: in_accel_z_en
Enabling: in_anglvel_x_en
/sys/bus/iio/devices/iio:device0 mpu6050-dev0
0.260728 9.966269 0.776204 -0.006388 -0.001065 -0.009583 946689166764774660
0.254748 9.972248 0.785772 -0.007453 -0.001065 -0.009583 946689166784769180
Disabling: in_accel_y_en
Disabling: in_anglvel_z_en
Disabling: in_accel_x_en
Disabling: in_timestamp_en
Disabling: in_anglvel_y_en
Disabling: in_accel_z_en
Disabling: in_anglvel_x_en
```

Now, the accelerometer is detected by kernel, also registered with iio interface. You can run `iiod` on the target with network backend enabled, this is accessible on local network for a libiio client (see `libiio/examples/iio-monitor` on the libiio github repo).

If you are getting an `Unable to create thread pool` (caused by `eventfd(0, EFD_NONBLOCK);` call) error while running `iiod`, you need to enable `EVENTFD` in `make linux-menuconfig`.
