---
title: "A Look at Raspberry Pi Camera Component in Home Assistant"
date: 2019-07-30T18:55:01+03:00
draft: false
tags: [ "homeassistant", "python" ]
---

In this post I will look at the `rpi_camera` component in Home Assistant to get a better understanding how the image from Raspberry Pi gets displayed in browser.

To setup this component add the following in `configuration.yaml`:
```
homeassistant:
  whitelist_external_dirs:
  - "/home/adrian/Downloads"

camera:
  - platform: rpi_camera
    timelapse: 1000
    file_path: "/home/adrian/Downloads/image.jpg"
```
And to `lovelace.yaml`:
```
      - type: picture-entity
        entity: camera.raspberry_pi_camera
```
Also don't forget to add the `homeassistant` user to the `video` group:
```
sudo usermod -a -G video homeassistant
```

As an overview on how this works:
- `raspistill` utility is started in `timelapse` mode as an independent child process (continuously takes a picture every `timelapse` ms), and overwrites each image at `file_path` (defined above)
- `RaspberryCamera.camera_image()` is called periodically, which reads the binary content of  `file_path` and returns it

### Requirements

This integration uses the `raspistill` to store the image from camera. `raspistill` is the command line tool for capturing still photographs with the camera module. It should be available by default on raspbian.

### Setup platform

When Home Assistant starts, `setup_platform` is called:
```python
def setup_platform(hass, config, add_entities, discovery_info=None):
    """Set up the Raspberry Camera."""
    if shutil.which("raspistill") is None:
        _LOGGER.error("'raspistill' was not found")
        return False

    setup_config = {
        CONF_NAME: config.get(CONF_NAME),
        CONF_IMAGE_WIDTH: config.get(CONF_IMAGE_WIDTH),
        CONF_IMAGE_HEIGHT: config.get(CONF_IMAGE_HEIGHT),
        CONF_IMAGE_QUALITY: config.get(CONF_IMAGE_QUALITY),
        CONF_IMAGE_ROTATION: config.get(CONF_IMAGE_ROTATION),
        CONF_TIMELAPSE: config.get(CONF_TIMELAPSE),
        CONF_HORIZONTAL_FLIP: config.get(CONF_HORIZONTAL_FLIP),
        CONF_VERTICAL_FLIP: config.get(CONF_VERTICAL_FLIP),
        CONF_FILE_PATH: config.get(CONF_FILE_PATH),
    }

    hass.bus.listen_once(EVENT_HOMEASSISTANT_STOP, kill_raspistill)

    file_path = setup_config[CONF_FILE_PATH]

    def delete_temp_file(*args):
        """Delete the temporary file to prevent saving multiple temp images.

        Only used when no path is defined
        """
        os.remove(file_path)

    # If no file path is defined, use a temporary file
    if file_path is None:
        temp_file = NamedTemporaryFile(suffix=".jpg", delete=False)
        temp_file.close()
        file_path = temp_file.name
        setup_config[CONF_FILE_PATH] = file_path
        hass.bus.listen_once(EVENT_HOMEASSISTANT_STOP, delete_temp_file)

    # Check whether the file path has been whitelisted
    elif not hass.config.is_allowed_path(file_path):
        _LOGGER.error("'%s' is not a whitelisted directory", file_path)
        return False

    add_entities([RaspberryCamera(setup_config)])
```
The code is quite clear:
- first checks if `raspistill` utility is available (on raspbian it should be)
- than the `setup_config` is retrived, mine looks like:
```python
{'name': 'Raspberry Pi Camera', 'image_width': 640, 'image_height': 480, 'image_quality': 7, 'image_rotation': 0, 'timelapse': 1000, 'horizontal_flip': 0, 'vertical_flip': 0, 'file_path': '/home/adrian/Downloads/image.jpg'}
```
- listens for the `EVENT_HOMEASSISTANT_STOP` event (which gets triggered when Home Assistant stops) to kill the running `raspistill` process
- checks `file_path` (I defined it in `configuration.yaml` as you can see above) and if it is whitelisted (it is as you can see in the `configuration.yaml`)
- creates an instance of the `RaspberryCamera(setup_config)` with the `setup_config` as argument, and gets registered with Home Assistant.


### The `RaspberryCamera` class
```python
class RaspberryCamera(Camera):
    """Representation of a Raspberry Pi camera."""

    def __init__(self, device_info):
        """Initialize Raspberry Pi camera component."""
        super().__init__()

        self._name = device_info[CONF_NAME]
        self._config = device_info

        # Kill if there's raspistill instance
        kill_raspistill()

        cmd_args = [
            "raspistill",
            "--nopreview",
            "-o",
            device_info[CONF_FILE_PATH],
            "-t",
            "0",
            "-w",
            str(device_info[CONF_IMAGE_WIDTH]),
            "-h",
            str(device_info[CONF_IMAGE_HEIGHT]),
            "-tl",
            str(device_info[CONF_TIMELAPSE]),
            "-q",
            str(device_info[CONF_IMAGE_QUALITY]),
            "-rot",
            str(device_info[CONF_IMAGE_ROTATION]),
        ]
        if device_info[CONF_HORIZONTAL_FLIP]:
            cmd_args.append("-hf")

        if device_info[CONF_VERTICAL_FLIP]:
            cmd_args.append("-vf")

        subprocess.Popen(cmd_args, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

    def camera_image(self):
        """Return raspistill image response."""
        with open(self._config[CONF_FILE_PATH], "rb") as file:
            return file.read()

    @property
    def name(self):
        """Return the name of this camera."""
        return self._name
```
Inherits from `Camera` and does the following:
- `kill`s any running `rsapistill` process
- sets up the arguments list for `raspistill` according to the received config. My `cmd_args` looks like this:
```python
['raspistill', '--nopreview', '-o', '/home/adrian/Downloads/image.jpg', '-t', '0', '-w', '640', '-h', '480', '-tl', '1000', '-q', '7', '-rot', '0']
```
	- `--nopreview` Do not display a preview window.
	- `'-o', '/home/adrian/Downloads/image.jpg'` outputs the image to specified file.
	- `'-t', '0'` Time (in ms) before takes picture and shuts down.
	- `'-w', '640', '-h', '480'` set image with and height.
	- `'-tl', '1000'` Timelapse mode. Takes a picture every <t>ms.
	- `'-q', '7'` Set jpeg quality <0 to 100>
	- `'-rot', '0'` Set image rotation (0-359).
- starts the `raspistill` process with the `cmd_args` values from the configuration.yaml:
```python
subprocess.Popen(cmd_args, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
```
`subprocess.Popen()` executes a child program in a new process. In this case `args` is a list of arguments. The program to execute (`raspistill`) is the first item in `args`. `stdout` (standard output) of this command is redirected to `/dev/null` and the `stderr` (standard error) is redirected to standard output to be logged if any error happens.

The `camera_image()` function, opens the saved image at `CONF_FILE_PATH` (`/home/adrian/Downloads/image.jpg` in my case), reads it as a binary file, and returns the content.
