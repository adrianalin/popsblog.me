---
title: "Build Linux for STM32F769I DISCO Using Buildroot"
date: 2020-02-02T18:55:01+03:00
draft: false
tags: [ "linux", "buildroot" , "stm32"]
---

Buildroot is a tool that simplifies and automates the process of building a complete Linux system for an embedded system, using cross-compilation. In order to achieve this, Buildroot is able to generate a cross-compilation toolchain, a root filesystem, a Linux kernel image and a bootloader for your target. Buildroot can be used for any combination of these options, independently (you can for example use an existing cross-compilation toolchain, and build only your root filesystem with Buildroot).
Recently I bought a nice little STM32F769I_DISC0 board, and looked into botting linux on it using buildroot.

![](https://raw.githubusercontent.com/adrianalin/STM32F769I-disco_Buildroot/master/stm32f769i-disco.jpg)

I started from https://github.com/fdu/STM32F769I-disco_Buildroot , and used buildroot 2020.05, kernel (5.6), u-boot (2020.01), busybox (1.31). Defconfigs were available for:

- buildroot: `./configs/stm32f469_disco_defconfig`
- linux: `./arch/arm/configs/stm32_defconfig`, and `./arch/arm/boot/dts/stm32f769-disco.dtb`
- u-boot: `./configs/stm32f769-disco_defconfig`

After cloning https://github.com/adrianalin/STM32F769I-disco_Buildroot, you need to setup a `tftp` server for hosting the image.


### `tftp` server setup:
- `apt install tftpd-hpa`
- edit `/etc/default/tftpd-hpa`:
```bash
TFTP_USERNAME="<username>"
TFTP_DIRECTORY="/srv/tftp/"
TFTP_ADDRESS=":69"
TFTP_OPTIONS="--ipv4 --secure"
```

These files should be provided by tftp server:
```bash
$ ls /srv/tftp/stm32f769/
stm32f769-disco.dtb  zImage
```

### Build the image

- `make bootstrap`: clones buildroot, and copies buildroot config
- `make build`: builds uboot-spl, uboot, kernel image and dtb, and copies zImage and dtb to `/srv/tftp/stm32f769/`
- `make flash_bootloader`, connect your board (ST-LINK micro USB connector) to PC before running this
- `sudo screen /dev/ttyACM0 115200` serial via STLINK connector, you should see the u-boot prompt (press User reset onboard button):

```bash
Trying to boot from XIP


U-Boot 2020.01 (Feb 02 2020 - 17:36:10 +0200)

Model: STMicroelectronics STM32F769-DISCO board
DRAM:  16 MiB
set_rate not implemented for clock index 4
set_rate not implemented for clock index 4
set_rate not implemented for clock index 4
Flash: 1 MiB
MMC:   sdio2@40011c00: 0
In:    serial
Out:   serial
Err:   serial
usr button is at LOW LEVEL
Net:
Warning: ethernet@40028000 (eth0) using random MAC address - 56:a7:f4:77:e5:97
eth0: ethernet@40028000
Hit SPACE in 3 seconds to stop autoboot.
Card did not respond to voltage select!
U-Boot >
```

### Building existing Linux repo using Buildroot

The normal operation of Buildroot is to download a tarball, extract it, configure, compile and install the software component found inside this tarball. The source code is extracted in `output/build/<package>-<version>`, which is a temporary directory: whenever make clean is used, this directory is entirely removed, and re-created at the next make invocation. Even when a Git or Subversion repository is used as the input for the package source code, Buildroot creates a tarball out of it, and then behaves as it normally does with tarballs.

However, if one uses Buildroot during the development of certain components of the system, this behavior is not very convenient: one would instead like to make a small change to the source code of one package, and be able to quickly rebuild the system with Buildroot.

Therefore, Buildroot provides a specific mechanism for this use case: the `<pkg>_OVERRIDE_SRCDIR` mechanism. Buildroot reads an override file, which allows the user to tell Buildroot the location of the source for certain packages.

To achieve this, Buildroot will use `rsync` to copy the source code of the component from the specified `<pkg>_OVERRIDE_SRCDIR` to `output/build/<package>-custom/`.

The default location of the override file is `$(CONFIG_DIR)/local.mk`, as defined by the `BR2_PACKAGE_OVERRIDE_FILE` configuration option. `$(CONFIG_DIR)` is the location of the Buildroot `.config` file, so `local.mk` by default lives side-by-side with the `.config` file.

For example my linux repo, where I do all the hacking and changes and commits, resides in `~/Downloads/linux`, and I have my `local.mk`:
```bash
LINUX_OVERRIDE_SRCDIR="/home/adrian/Downloads/linux"
```

After making changes in the linux repo, run:
```bash
make linux-rebuild all
```
and in a matter of seconds you get the updated Linux kernel image in `output/images`.

This can be done with any package, not just Linux. See [Buildroot manual](https://buildroot.org/downloads/manual/manual.html#_developer_guide) section `Using Buildroot during development`

### Booting Linux

After successfully getting to u-boot console, make sure tftp server is accessible , connect your board to ethernet and run:
```bash
U-Boot > dhcp
U-Boot > setenv serverip 192.168.0.122
U-Boot > tftp 0xc0300000 stm32f769/stm32f769-disco.dtb && tftp 0xc0000000 stm32f769/zImage && bootz 0xc0000000 - 0xc0300000
```

And you should see linux kernel booting, and the green user LED 'heartbeating'.

```bash
Welcome to Buildroot
buildroot login: root
Jan  1 00:00:07 login[43]: root login on 'console'


BusyBox v1.31.1 (2020-02-02 17:32:11 EET) hush - the humble shell
Enter 'help' for a list of built-in commands.
~ # cat /proc/version 
Linux version 5.6.15 (adrian@adrian-ux310uqk) (gcc version 8.4.0 (Buildroot 2020.05))
```

### What's working:

#### Ethernet

Driver is already available in kernel, only needed to enable the configs and add the hardware in device tree.
```bash
~ # ifconfig eth0 up 192.168.0.123
~ # ping 192.168.0.122
PING 192.168.0.122 (192.168.0.122): 56 data bytes
64 bytes from 192.168.0.123: icmp_seq=1 ttl=64 time=3.95 ms
64 bytes from 192.168.0.123: icmp_seq=2 ttl=64 time=2.83 ms
64 bytes from 192.168.0.123: icmp_seq=3 ttl=64 time=1.91 ms
64 bytes from 192.168.0.123: icmp_seq=4 ttl=64 time=2.62 ms
64 bytes from 192.168.0.123: icmp_seq=5 ttl=64 time=5.28 ms
64 bytes from 192.168.0.123: icmp_seq=6 ttl=64 time=2.14 ms
```

#### I2C IIO MPU6050

[STM32F769 DISCO Linux and IIO Sensors](https://adrianalin.gitlab.io/popsblog.me/posts/stm32f769-disco-linux-and-iio-sensors/)

#### DFSDM ADC

STM32 DFSDM ADC is a sigma delta analog-to-digital converter dedicated to interface external sigma delta modulators to STM32 micro controllers. (See `bindings/adc/iio/st,stm32-dfsdm-adc.txt`)

It is mainly targeted for:

- Sigma delta modulators (motor control, metering...)
- PDM microphones (audio digital microphone)

There is a patch `linux.002_dfsdm_adc.patch` on the repo that enables DFSDM ADC in devicetree. This enables DFSDM DATIN1 channel (connected to U5 top left and U6 top right microphones), with filter 0, and sigma delta modulator.

Also need to enable `CONFIG_STM32_DFSDM_ADC` in linux menuconfig. The device should now be registered with iio interface in `/sys/bus/iio/devices/iio\:device1`. This is not very useful, because you just get a raw voltage value from micophone, but it's still good enough to experiment. When reading data from it, you can use a logic analyzer to capture the clock signal on TP5, and PDM microphone signal on TP103 test points.

#### Audio codec WM8994 (still need to figure it out)

[Alsa on STM32F769 disco with WM8994](https://adrianalin.gitlab.io/popsblog.me/posts/alsa-on-stm32f769-disco-with-wm8994/)

#### LCD display framebuffer `/dev/fb0`

[STM32F769I-DISCO otm8009 linux framebuffer](https://adrianalin.gitlab.io/popsblog.me/posts/stm32f769i-disco-otm8009-linux-framebuffer/)

#### Touchscreen

In `stm32f7-pinctrl.dtsi`, configure I2C pins used to handle the touchscreen:
```
			i2c4_pins: i2c4@0 {
				pins {
					pinmux = <STM32_PINMUX('D', 12, AF4)>, /* I2C4 SCL */
						<STM32_PINMUX('B', 7, AF11)>; /* I2S4 SDA */
					bias-disable;
					drive-open-drain;
				};
			};
```

In `stm32f769-disco.dts` enable the touchscreen:
```
&i2c4 {
	pinctrl-0 = <&i2c4_pins>;
	pinctrl-names = "default";
	status = "okay";
	touchscreen@2a {
		compatible = "focaltech,ft6236"; // ft6206 ?
		reg = <0x2a>;
		interrupts = <13 2>;
		interrupt-parent = <&gpioi>;
		interrupt-controller;
		touchscreen-size-x = <480>;
		touchscreen-size-y = <800>;
		status = "okay";
	};
};
```

Enable the following in `make linux-menuconfig`:
```
CONFIG_INPUT_EVDEV=y
CONFIG_INPUT_TOUCHSCREEN=y
CONFIG_TOUCHSCREEN_EDT_FT5X06=y
```

The result can be tested with `evtest` https://gitlab.freedesktop.org/libevdev/evtest (enable with BR2_PACKAGE_EVTEST in `buildroot menuconfig`).

#### Micro SD card

It's possible to write the rootfs on sd card (as r/w), and load `zImage` from `tftp`. See https://adrianalin.gitlab.io/popsblog.me/posts/linux-boot-options-on-stm32f769i-disco/.


#### Linux kernel Debugging

For debugging you can use `st-util` or `openocd`. I noticed that `st-util` works better for me but hangs on `step` or `next` (same with `openocd`).

Because I have some very long paths, I put them in environment variables in `~/.bashrc`:
```bash
export STM32F7_CFG=/home/<your_user>/stm32f7/STM32F769I-disco_Buildroot/buildroot/output/build/host-openocd-0.10.0/tcl/board/stm32f7discovery.cfg

export VMLINUX=/home/<your_user>/stm32f7/STM32F769I-disco_Buildroot/buildroot/output/build/linux-5.4.10/vmlinux

export STLINK_CFG=/home/<your_user>/stm32f7/STM32F769I-disco_Buildroot/buildroot/output/host/share/openocd/scripts/interface/stlink-v2-1.cfg
```

* Run `st-util` on your host.
* It should recognize connected board:

```bash
2020-03-25T13:44:49 INFO gdb-server.c: Listening at *:4242...
2020-03-25T13:44:53 INFO gdb-server.c: Found 8 hw breakpoint registers
2020-03-25T13:44:53 INFO gdb-server.c: GDB connected.
```
* add `.gdbinit` in current directory to autoconnect to gdb server:
```bash
target extended-remote :4242
```

* `$ gdb-multiarch $VMLINUX`. Now you can run gdb commands like: set breakpoint at line (`hb file.c:1222`), call stack (`backtrace`), print variable (`p variable`), restart program (`kill`, `run`), `list`, `up`, `down` (move up and down the stack).

If you prefer `openocd`:

* Reboot the board to `u-boot`.
* Run `openocd`:

```bash
$ openocd -f $STLINK_CFG -f $STM32F7_CFG
```

* Attach the board with gdb:

```bash
$ gdb-multiarch $VMLINUX
(gdb) target remote :3333
Remote debugging using :3333
0x00000000 in ?? ()
```

* Set a breakpoint:

```bash
(gdb) hb inv_mpu_core_probe
Hardware assisted breakpoint 1 at 0xc01829ac: file drivers/iio/imu/inv_mpu6050/inv_mpu_core.c, line 1073.
```

* In `u-boot` console start the kernel:

```bash
U-Boot > run netboot
```

#### Troubleshoot
1. If you're getting stuck at `Starting kernel ...`, make sure that U-Boot bootargs look like this:
```bash
U-Boot > printenv
bootargs=console=ttySTM0,115200 earlyprintk consoleblank=0 ignore_loglevel
```

#### Transfer file via serial console using `cu`

It's better to base64 encode the file before sending via serial, because may contain characters that may close the connection eariler.

Connect to remote board (raspberry pi is the host, STM32 is the target connected to serial using serial):
```bash
pi@raspberrypi:~ $ cu -l /dev/ttyACM0 -s 115200
Connected.
```

Transfer from target to local:
```bash
~ # ~t screenshot.bmp
Local file name [screenshot.bmp]:
[file transfer complete]
[connected]
```

Transfer file from local to target:
```bash
~ # ~p screenshot.bmp
Remote file name [screenshot.bmp]:
1
[file transfer complete]
[connected]
```
