---
title: "STM32F769I DISCO Otm8009 Linux Framebuffer"
date: 2020-03-26T18:55:02+03:00
draft: false
tags: [ "linux" , "stm32", "buildroot"]
---

The frame buffer device provides an abstraction for the graphics hardware. It represents the frame buffer of some video hardware and allows application software to access the graphics hardware through a well-defined interface, so the software doesn't need to know anything about the low-level (hardware register) stuff.

The device is accessed through special device nodes, usually located in the `/dev` directory, i.e. `/dev/fb*`. For more info see `Documentation/fb/framebuffer.txt`, `Documentation/fb/framebuffer.rst`.

This board features an LCD display MIPI DSI, 4-inch 800x480 LCD-TFT with capacitive touch panel, schematics available in `UM2033 User manual`. Also useful: `Documentation/devicetree/bindings/display/stm,stm32-ltdc.txt`, `Documentation/devicetree/bindings/display/panel/orisetech.otm8009a.txt`.

Options that need to be enabled in the kernel: `CONFIG_DRM`, `CONFIG_DRM_STM`, `CONFIG_DRM_STM_DSI`, `DRM_PANEL_ORISETECH_OTM8009A`. There are also changes in the devicetree that configure the screen, see `linux.003_video_framebuffer.patch` on the repo.

I also enabled `BR2_PACKAGE_FB_TEST_APP` in buildroot, a test suite for Linux framebuffer, and you can run for example `fb-test-rect` that displays random rectangles on the screen. Uses `ioctl` to retrieve fb info, like:

```bash
var.xres_virtual: 480
var.yres_virtual: 800
fix.line_length: 1024
800 * 1024 = 819200 // this is the framebuffer size
var.bits_per_pixel : 16
var.red.length, var.red.offset: length 5 bits, offset 11
var.green.length, var.green.offset: length 6 bits, offset 5
var.blue.length, var.blue.offset: length 5 bits, offset 0
```

`mmap` (the driver `fops` interface provides `mmap` API) maps the video framebuffer, as a result the application gets a pointer to the framebuffer memory, any changes to this memory are reflected on the display.

After Linux boots, `/dev/fb0` should be accessible. `/sys/class/backlight/40016c00.dsi.0/brightness` can be used for controlling brightness. You first need to run `echo 0 0 > /sys/class/graphics/fb0/pan` to enable the framebuffer. Than you can run `fb-test-rectangle`.

More info can be found in `include/uapi/linux/fb.h` (Linux tree).

### Video mode patern generator (VPG)

The video mode pattern generator allows the transmission of horizontal/vertical color bar and D-PHY BER testing pattern without any stimuli. The color bar pattern comprises eight bars for white, yellow, cyan, green, magenta, red blue, and black colors.

If you look in `dw-mipi-dsi.c` you will find that `ifdef CONFIG_DEBUG_FS` adds information regarding the `vpg` to debugfs.

Mount the debug fs, enable the `vpg`, set orientation, enable the framebuffer, and see the color pattern shown on display:

```bash
mount -t debugfs none /sys/kernel/debug
echo 1 > /sys/kernel/debug/40016c00.dsi/vpg
echo 1 > /sys/kernel/debug/40016c00.dsi/vpg_horizontal
echo 0 0 > /sys/class/graphics/fb0/pan
```


![](https://raw.githubusercontent.com/adrianalin/STM32F769I-disco_Buildroot/master/pictures/vpg_picture.jpg)


### Enable the framebuffer console

Enable fbcon in linux kernel `VT=y FRAMEBUFFER_CONSOLE=Y`.

I also enabled libdrm in buildroot in order to play with `modetest`, but when I run:
```bash
~ # modetest --help
binfmt_flat: reference 0xffa424 to shared library 127, killing modetest!
SEGV
```
This is a nommu platform, and has some limitations, like the number of shared librarie is limited to 4 `CONFIG_BINFMT_SHARED_FLAT`. There are more details about uCLinux in `Embedded Linux System Design and Development` book.

μClinux is a variation of the Linux kernel, previously maintained as a fork, that targets microcontrollers without a memory management unit (MMU).[1] It was integrated into the mainline of development as of 2.5.46;[2] the project continues to develop patches and tools for microcontrollers [https://en.wikipedia.org/wiki/%CE%9CClinux].

### SDL test

Simple DirectMedia Layer - SDL is a library that allows programs portable low level access to a video framebuffer, audio output, mouse, and keyboard. In buildroot you can enable SDL version 1.2.15, with framebuffer console for rendering. It uses ioctl with `/dev/fb0`, I recommend looking in the SDL source file `SDL_fbvideo.c` for more details about how it interacts with the kernelspace driver.

To enable SDL, in buildroot menuconfig add:
```bash
BR2_PACKAGE_SDL=y
BR2_PACKAGE_SDL_FBCON=y
BR2_PACKAGE_SDL_TTF=y # this can be use to draw text on screen
```

Add `fbcon=map:1` to bootargs. Because there is only one driver available (`/dev/fb0`), `fbcon=map:1` tells fbcon not to take over the console.
In order to execute the SDL test app run:
```bash
~ # SDL_NOMOUSE=1 hello
```
![](https://raw.githubusercontent.com/adrianalin/STM32F769I-disco_Buildroot/master/pictures/sdl_picture.jpg)

The `hello` app, is a simple cmake, C++11 app, that uses SDL (statically linked) to draw a bitmap or some text on screen (for text drawing, you need to load .ttf font with SDL_ttf). Because currently I use an initramfs (kernel and filesystem loaded in memory), there is not much memory left for application (total onboard RAM memory 16MB). It would be nice to have a XIP kernel in place, and the rootfs on SD card, so more memory will be available. Since this uses cmake, you can open the application with QtCreator, and test on desktop first before deploying to target.

![Rootfs size](https://raw.githubusercontent.com/adrianalin/STM32F769I-disco_Buildroot/master/pictures/filesystem_size.png)

In order to preserve memory I think I will move away from C++ to C.

The `hello` app can be found in the repo at `package/hello`. When running the app, the bmp should be displayed. There is also an event triggered when you press the onboard blue push button.

### `regmap` register dump

Debugfs contains a register cache (mirror) for drivers/peripherals based on regmap API https://elinux.org/images/a/a3/Regmap-_The_Power_of_Subsystems_and_Abstractions.pdf.

Regmap is a register map abstraction API of the Linux kernel. It is mainly used for serial bus device drivers (I2C and SPI), but can also be used for internal peripheral drivers.

Many of these drivers contain some very similar code for accessing connected-hardware device registers.
The regmap kernel API proposes a solution which factors out this code from the drivers, saving code and making it much easier to share infrastructure. (https://wiki.st.com/stm32mpu/wiki/Debugfs)

After mounting the debugfs:
```bash
~ # ls /sys/kernel/debug/regmap/40016c00.dsi-dw-mipi-dsi/
name       range      registers  access
~ # cat /sys/kernel/debug/regmap/40016c00.dsi-dw-mipi-dsi/registers
000: 3133302a
004: 00000001
008: 00000a03
00c: 00000000
010: 00000005
014: 00000000
018: 00040004
...
430: 01000839
```
The first column representes the register address, and the second column the register value. For register definitions, please refer to the device specification.
Note: In order to access the register dump this way, the driver must use `regmap`, and set `regmap_config.max_register` field (in this case is 0x430, see `dw-mipi-dsi.c`). This functionality is implemented in `regmap-debugfs.c`.

There is also `busybox devmem` but crashes on my target (STM32F769) .This can be enabled using buildroot, run `make busybox-menuconfig`.

### Debugging the `drm`

To enable debugging:
```bash
~ # echo 0xff > /sys/module/drm/parameters/debug
```

When running, the output should look like:
```bash
~ # echo 0 0 > /sys/class/graphics/fb0/pan
[drm:0xc013fd19] Allocated atomic state (ptrval)
[drm:0xc0140035] Added [PLANE:33:plane-0] (ptrval) state to (ptrval)
[drm:0xc0140035] Added [PLANE:36:plane-1] (ptrval) state to (ptrval)
[drm:0xc014c723] Set [NOFB] for [PLANE:36:plane-1] state (ptrval)
[drm:0xc013ff77] Added [CRTC:35:crtc-0] (ptrval) state to (ptrval)
[drm:0xc014c533] Set [MODE:480x800] for [CRTC:35:crtc-0] state (ptrval)
[drm:0xc014c6bd] Link [PLANE:33:plane-0] state (ptrval) to [CRTC:35:crtc-0]
[drm:0xc014c703] Set [FB:38] for [PLANE:33:plane-0] state (ptrval)
[drm:0xc01440e1] OBJ ID: 38 (1)
[drm:0xc0140395] Adding all current connectors for [CRTC:35:crtc-0] to (ptrval)
[drm:0xc01440e1] OBJ ID: 32 (2)
[drm:0xc01402d3] Added [CONNECTOR:32:DSI-1] (ptrval) state to (ptrval)
[drm:0xc01440e1] OBJ ID: 32 (3)
[drm:0xc014c7b1] Link [CONNECTOR:32:DSI-1] state (ptrval) to [CRTC:35:crtc-0]
[drm:0xc0140479] checking (ptrval)
[drm:0xc012f693] [CRTC:35:crtc-0] mode changed
[drm:0xc012f6b1] [CRTC:35:crtc-0] enable changed
[drm:0xc012f6d3] [CRTC:35:crtc-0] active changed
[drm:0xc012f755] Updating routing for [CONNECTOR:32:DSI-1]
[drm:0xc012f95d] [CONNECTOR:32:DSI-1] using [ENCODER:31:DPI-31] on [CRTC:35:crtc-0]
[drm:0xc012f9af] [CRTC:35:crtc-0] needs all connectors, enable: y, active: y
[drm:0xc0140395] Adding all current connectors for [CRTC:35:crtc-0] to (ptrval)
[drm:0xc0140415] Adding all current planes for [CRTC:35:crtc-0] to (ptrval)
[drm:0xc0140319] Adding all bridges for [encoder:31:DPI-31] to (ptrval)
[drm:0xc014016b] Added new private object (ptrval) state (ptrval) to (ptrval)
[drm:0xc015056b] clk rate target 29700000, available 24375000
[drm:0xc0150c75] requested clock 29700kHz, adjusted clock 24375kHz
[drm:0xc0150313] 
[drm:0xc0150313] 
[drm:0xc01408c1] committing (ptrval)
[drm:0xc0147dc1] crtc 35: hwmode: htotal 708, vtotal 839, vdisplay 800
[drm:0xc0147dd3] crtc 35: clock 29700 kHz framedur 20000404 linedur 23838
[drm:0xc013014b] modeset on [CRTC:35:crtc-0]
[drm:0xc0150069] 
[drm:0xc0150cb5] 
[drm:0xc015045d] CRTC:35 mode:480x800
[drm:0xc0150469] Video mode: 480x800
[drm:0xc0150485]  hfp 98 hbp 98 hsl 32 vfp 15 vbp 14 vsl 10
[drm:0xc0130191] modeset on [ENCODER:31:DPI-31]
[drm:0xc01503b7] 
---dw_mipi_dsi_mode_set 1048 // this is debug message i added
[drm:0xc014fffb] pll_in 25000kHz pll_out 350000kHz lane_mbps 350MHz
[drm:0xc0153531] failed to wait phy lock state // THIS is weird
[drm:0xc014fc39] 
[drm:0xc0150751] plane:33 fb:38 (480x800)@(0,0) -> (480x800)@(0,0)
[drm:0xc01508ab] fb: phys 0xc0ef1000
[drm:0xc0150a95] 
[drm:0xc01502b3] 
[drm:0xc01477e1] enabling vblank on crtc 0, ret: 0
[drm:0xc0147f9b] crtc 0 : v p(0,-23)@ 98.360364 -> 98.360912 [e 1 us, 0 rep]
[drm:0xc01476a5] crtc 0: Calculating number of vblanks. diff_ns = 98360912622, framedur_ns = 20000404)
[drm:0xc01476ff] updating vblank count on crtc 0: current=0, diff=4918, hw=0 hw_last=0
[drm:0xc0147f9b] crtc 0 : v p(0,-23)@ 98.360501 -> 98.361049 [e 2 us, 0 rep]
[drm:0xc01476a5] crtc 0: Calculating number of vblanks. diff_ns = 137000, framedur_ns = 20000404)
[drm:0xc01476ff] updating vblank count on crtc 0: current=4918, diff=0, hw=0 hw_last=0
[drm:0xc013026d] enabling [CRTC:35:crtc-0]
[drm:0xc01503dd] 
[drm:0xc014842f] crtc 0, vblank enabled 1, inmodeset 0
[drm:0xc0147f9b] crtc 0 : v p(0,-23)@ 98.360696 -> 98.361244 [e 1 us, 0 rep]
[drm:0xc01302bf] enabling [ENCODER:31:DPI-31]
[drm:0xc015026f] 
[drm:0xc0147f9b] crtc 0 : v p(0,-32)@ 98.699036 -> 98.699799 [e 2 us, 0 rep]
[drm:0xc01476a5] crtc 0: Calculating number of vblanks. diff_ns = 338554726, framedur_ns = 20000404)
[drm:0xc01476ff] updating vblank count on crtc 0: current=4919, diff=17, hw=0 hw_last=0
[drm:0xc0148b93] vblank event on 4919, current 4936
[drm:0xc013fd83] Clearing atomic state (ptrval)
[drm:0xc01440af] OBJ ID: 32 (4)
[drm:0013fedd] Freeing atomic state (ptrval)
```


### Resources

[Build Linux for STM32F76-DISCO](https://adrianalin.gitlab.io/popsblog.me/posts/build-linux-for-stm32f769i-disco-using-buildroot/)

https://github.com/adrianalin/STM32F769I-disco_Buildroot

https://wiki.st.com/stm32mpu/wiki/DRM_KMS_overview

[Back to the linux Framebuffer](https://fosdem.org/2020/schedule/event/fbdev/attachments/slides/3595/export/events/attachments/fbdev/slides/3595/fosdem_2020_nicolas_caramelli_linux_framebuffer.pdf)

