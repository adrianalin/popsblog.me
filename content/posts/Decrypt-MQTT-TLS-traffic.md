---
title: "Decrypt MQTT TLS traffic"
date: 2021-07-14T14:43:29+03:00
draft: false
tags: [ MQTT ]
---

I have a MQTT broker on Raspberry Pi (client) which connects to one on my laptop (server) with TLS encrypted communication, this is known as MQTT bridge. As MQTT broker I used `mosquitto`. Here I will shortly describe how to do this setup, capture the traffic between the two, and decrypt with Wireshark (version 3.4.6).

For my testing I needed to use an older version of `mosquitto` v1.4.14.

My setup implies following steps:
* start Wireshark capture on laptop
* configure (mosquitto.conf), modify, recompile run changed `mosquitto` broker on laptop
* configure (mosquitto.conf) and start `mosquitto` broker on RPI configured as a bridge
* on laptop (server), the key log is printed, save to a file, and load it in Wireshark (is used for decryption)
* read decrypted MQTT messages in Wireshark

### MQTT broker setup on laptop

#### Code changes in `mosquitto`

There is also a commit on mosquitto develop branch that already implemented this: https://github.com/eclipse/mosquitto/commit/a5eb2f25eb849b788d92ad721dd133b7f68afca1
It is important to print the session secrets needed later for decrypting with Wireshark:

```c
void key_callback(const SSL *ssl, const char *line)
{
	printf("%s\n", line);
}

int mqtt3_socket_accept(struct mosquitto_db *db, mosq_sock_t listensock)
{
	...
	SSL_CTX_set_keylog_callback(db->config->listeners[i].ssl_ctx, key_callback);
	...
}
```

When starting the MQTT bridge (running on Raspberry Pi, steps bellow), the `printf` line above outputs the session secrets for TLSv1.2:
```
CLIENT_RANDOM 6bbdd42471635c9d237c20d2f413e39490fd335b1c5a9016ace13e43f2131262 69958da5352df7b8af4b61e870222d26f35f6a00efc5c98fab0bed6
d20f3b44d998afe647a600c7db449aa916100c077
```

Edit mosquitto configuration (TLS/SSL certificates generation can be found online):

```bash
[miha@asus mosquitto-1.4.14]$ cat /etc/mosquitto/mosquitto.conf
...
log_type all
listener 1883
allow_anonymous true

listener 8883
protocol mqtt
cafile /etc/mosquitto/certs/ca.crt
keyfile /etc/mosquitto/certs/server.key
certfile /etc/mosquitto/certs/server.crt
...
```

Rebuild mosquitto and start specifying the above config:
```bash
[miha@asus mosquitto-1.4.14]$ ./src/mosquitto -c /etc/mosquitto/mosquitto.conf -v
1626264093: mosquitto version 1.4.14 (build date 2021-07-14 13:27:05+0300) starting
1626264093: Config loaded from /etc/mosquitto/mosquitto.conf.
1626264093: Opening ipv4 listen socket on port 1883.
1626264093: Opening ipv6 listen socket on port 1883.
1626264093: Opening ipv4 listen socket on port 8883.
1626264093: Opening ipv6 listen socket on port 8883.
```
When we start `mosquitto` on Raspberry Pi, it will connect:
```bash
1626264093: <><><><>With tls
1626264093: calling SSL_CTX_set_keylog_callback
1626264093: <><><><>New connection from 192.168.0.206 on port 8883.
CLIENT_RANDOM 6bbdd42471635c9d237c20d2f413e39490fd335b1c5a9016ace13e43f2131262 69958da5352df7b8af4b61e870222d26f35f6a00efc5c98fab0bed6
d20f3b44d998afe647a600c7db449aa916100c077
1626264094: Client raspberrypi.bridge-01 disconnected.
1626264094: New client connected from 192.168.0.206 as raspberrypi.bridge-01 (c0, k60).
1626264094: Sending CONNACK to raspberrypi.bridge-01 (0, 0)
1626264094: Received PUBLISH from raspberrypi.bridge-01 (d0, q1, r1, m1, '$SYS/broker/connection/raspberrypi.bridge-01/state', ... (1 
bytes))
1626264094: Sending PUBACK to raspberrypi.bridge-01 (Mid: 1)
1626264094: Received UNSUBSCRIBE from raspberrypi.bridge-01
1626264094:     house/#
1626264094: raspberrypi.bridge-01 house/#
1626264153: Received PINGREQ from raspberrypi.bridge-01
1626264153: Sending PINGRESP to raspberrypi.bridge-01
```

### Raspberry Pi MQTT broker setup with encryption

Edit mosquitto configuration:
```bash
pi@raspberrypi:~/mosquitto-1.4.14 $ cat /etc/mosquitto/mosquitto.conf
...
connection bridge-01                       # bridge name
address 192.168.0.157:8883                 # remote MQTT broker, acts as a server
bridge_cafile /etc/mosquitto/certs/ca.crt  # generated certificates

topic house/# out                          # messages to this topic are forwarded to MQTT broker on laptop

log_type all # enable logs
```

Start mosquitto using the above configuration:
```bash
pi@raspberrypi:~/mosquitto-1.4.14 $ ./src/mosquitto -c /etc/mosquitto/mosquitto.conf -v
1626264140: mosquitto version 1.4.14 (build date 2021-07-14 07:15:42+0100) starting
1626264140: Config loaded from /etc/mosquitto/mosquitto.conf.
1626264140: Opening ipv4 listen socket on port 1883.
1626264140: Opening ipv6 listen socket on port 1883.
1626264140: Bridge local.raspberrypi.bridge-01 doing local SUBSCRIBE on topic house/#
1626264140: Connecting bridge bridge-01 (192.168.0.157:8883)
1626264140: Bridge raspberrypi.bridge-01 sending CONNECT
1626264141: Received CONNACK on connection local.raspberrypi.bridge-01.
1626264141: Bridge local.raspberrypi.bridge-01 sending UNSUBSCRIBE (Mid: 2, Topic: house/#)
1626264141: Received PUBACK from local.raspberrypi.bridge-01 (Mid: 1)
1626264141: Received UNSUBACK from local.raspberrypi.bridge-01
```

### Decryption

This is how encrypted traffic looks like:

![](https://adrianalin.gitlab.io/popsblog.me/images/MQTT_encrypted.png)

To decrypt, I saved the TLS session secrets mentioned above in a text file `secretlog.txt`. Note that session secrets changes at every restart.
For browsers same output is obtained by setting environment variable `SSLKEYLOGFILE` before starting, which appends secrets to a file in current path.

This file I loaded in Wireshark to decrypt the MQTT encrypted messages: `Edit -> Preferences -> Protocols -> TLS -> (Pre)-Master-Secret` log filename.
Also enable these TCP protocol preferences (`Edit -> Preferences -> Protocols -> TCP`):
* `Allow subdissector to reassemble TCP streams`
* `Reassemble out-of-order segments (since Wireshark 3.0, disabled by default)`

Decrypted result:

![](https://adrianalin.gitlab.io/popsblog.me/images/MQTT_decrypted.png)

More info about TLS/SSL decryption with Wireshark: https://www.youtube.com/watch?v=Ha4SLHceF6w, and https://lekensteyn.nl/files/wireshark-tls-debugging-sharkfest19us.pdf