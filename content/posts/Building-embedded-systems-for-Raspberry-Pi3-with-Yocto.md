---
title: "Building Embedded Systems for Raspberry Pi3 With Yocto"
date: 2020-01-21T18:55:01+03:00
draft: false
tags: [ "raspberrypi", "linux", "yocto" ]
---

Yocto is an open source project that helps you build your own minimal Linux distribution for embedded and IOT software, independent of the underlying architecture of the embedded hardware.

To quickly get started with my setup, clone https://github.com/adrianalin/meta-rpi.

### Setup WiFi on Raspberry Pi3

Edit `wpa_supplicant.conf` with your the wifi credentials.

Add the following to your base image `meta-rpi/images/console-basic-image.bb`:
```bash
include recipes-core/images/core-image-base.bb

IMAGE_FEATURES += "package-management splash"

CORE_OS = " \
    openssh openssh-keygen openssh-sftp-server \
"

WIFI_SUPPORT = " \
    crda \
    iw \
    linux-firmware-bcm43430\
    wpa-supplicant \
"

IMAGE_INSTALL += " \
    ${CORE_OS} \
    ${WIFI_SUPPORT} \
"

export IMAGE_BASENAME = "console-basic-image"
```

Note: when booting the image, random number generator takes some time to initialize.

### Using your workstation as a remote `opkg` repository

Embedded Linux built with Yocto adds `opkg` package manager on the target. During development, there are situations when you need to install new packages. Instead of adding the needed packages in your build system, and do a full image build and deploy again, you can just build needed packages and install them from your host.

* Copy the needed packages from `$(TMPDIR)/deploy/ipk` to a new directory `~/raspberry_pi/snapshots`:
```bash
~/raspberry_pi/snapshots$ ls
python3-pyqt5_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk
python3-pyqt5-dbg_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk
python3-pyqt5-dev_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk
python3-pyqt5-src_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk
```
* Need to create a manifest file using the `opkg-utils`:
```bash
~/raspberry_pi$ git clone git://git.yoctoproject.org/opkg-utils
~/raspberry_pi/snapshots$ ../opkg-utils/opkg-make-index . > Packages
~/raspberry_pi/snapshots$ ls
Packages                                             python3-pyqt5-dbg_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk
Packages.stamps                                      python3-pyqt5-dev_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk
python3-pyqt5_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk  python3-pyqt5-src_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk
~/raspberry_pi/snapshots$ cat Packages
Package: python3-pyqt5-dbg
Version: 5.12.1-r0
Recommends: libc6-dbg, libgcc-s-dbg, libstdc++-dbg, qtbase-dbg, qtdeclarative-dbg
Section: devel
Architecture: cortexa7t2hf-neon-vfpv4
Maintainer: Poky <poky@yoctoproject.org>
MD5Sum: 214bc6696daa24b6dee0d970fee297ff
Size: 46079384
Filename: python3-pyqt5-dbg_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk
Source: python3-pyqt5_5.12.1.bb
Description: Python Qt5 Bindings - Debugging files
OE: python3-pyqt5
HomePage: http://riverbankcomputing.co.uk
License: GPLv3
Priority: optional
...
~/raspberry_pi/snapshots$ gzip Packages
```
* In `~/raspberry_pi/` start the webserver: `python3 -m http.server 80`. 
* On target, edit `/etc/opkg/opkg.conf` and add:
```bash
src/gz snapshots http://192.168.1.123/snapshots
```

Above are my paths for raspberrypi3, you need to change accordingly, also you need use your host IP address.

When doing an update on target now, you get:
```bash
root@rpi3:~# opkg update
Downloading http://192.168.1.123/snapshots/Packages.gz.
Updated source 'snapshots'.
```

And you can install the new package:
```bash
root@rpi3:~# opkg install --force-reinstall python3-pyqt5
Downloading http://192.168.1.123/snapshots/python3-pyqt5_5.12.1-r0_cortexa7t2hf-neon-vfpv4.ipk.
Installing python3-pyqt5 (5.12.1) on root
Configuring python3-pyqt5.
```

### Useful debugging info with `bitbake`

* `bitbake-layer show-layers` to show all active layers with their priorities
* `bitbake-layers show-recipes` to list all reipies
* `bitbake -c cleanall libiio` - clean `libiio`;
* `bitbake -e qtbase | grep ^PACKAGECONFIG_GL` - bitbake environment variables; grep for `PACKAGECONFIG_GL` variable in `qtbase` recipe
* `bitbake-layers show-appends` to see what bbappends are used
* `bitbake -s` list all packages currently available in our recipes
* `bitbake -s | grep ^python3` thirdparty Python modules are provided as individual recipes, use this command to get a list of available modules

* `bitbake -f -c compile libiio` compile package `libiio` binary found in `~/raspberry_pi/rpi3aplus/build/tmp/work/cortexa7t2hf-neon-vfpv4-poky-linux-gnueabi/libiio/0.19+gitAUTOINC+5f5af2e417-r0/build/iiod`.

#### Modify existing bitbake recipe

* `devtool modify -x libiio` modify existing recipe; you can now add your changes in `build/workspace/sources/libiio`; you can `git add ...` and `git commit ...` your changes
* `devtool update-recipe libiio` update recipe after code changes are done
* `devtool edit-recipe libiio`
* `devtool build libiio`
* `devtool deploy-target libiio root@192.168.0.94` the target is a live target machine running as an SSH server
* `devtool finish recipe layer` moves the new recipe to a more permanent layer, and then resets the recipe so that the recipe is built normally rather than from the workspace.
* `devtool modify linux-raspberrypi` adds linux-raspberrypi to workspace and run `devtool build linux-raspberrypi`. There will be a new directory called `oe-workdir` in `workspace/sources/linux-raspberrypi/oe-workdir` that contains build output.

Simillar steps can be followed in order to modify the `linux-raspberrypi` linux kernel.

### Installing Python packages

Thirdparty Python modules are provided as individual recipes, run the following to get a list:
```bash
bitbake -s | grep ^python3
```

This list may be short and this is because only a small number of modules are provided by `openembedded-core`. There is also a package called `python3-modules` which will install all standard library modules.

I needed `python3-paho-mqtt` which I added to my `images/python3-qt5-image.bb`:
```bash
IMAGE_INSTALL += " \
	python3-paho-mqtt \
"
```

It is also always a good idea to search the [OpenEmbedded Layer Index](https://layers.openembedded.org/layerindex/branch/master/layers/) if you are looking for missing modules.

Another python package that i needed is `python-mpd2` which was not provided by the `meta-python`, so I needed to create a simple recipe for it:
```bash
SUMMARY = "A Python MPD client library"
HOMEPAGE = "https://github.com/Mic92/python-mpd2"
LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=e6a600fd5e1d9cbde2d983680233ad02"

SRC_URI = "https://files.pythonhosted.org/packages/a8/12/63bdb3ee8e0bd7dd0476e79f0f130c1caeff408a1b1e5531ae9891805f7d/python-mpd2-1.0.0.tar.bz2"
SRC_URI[md5sum] = "53a67260a39865bb12e4fbb570597fa0"

PYPI_PACKAGE = "python-mp2"

inherit setuptools3

RDEPENDS_${PN} += " \
"
```

As you can see the recipe is quite small due to `setuptools3` classes which do all the hard work for us.


### Touchscreen config

Add `qt5_env.sh`.

To enable the PiTFT add to `/boot/config.txt`:
```bash
dtparam=spi=on
dtoverlay=pitft35-resistive,rotate=0,speed=32000000,fps=60
```
Install `tslib`, `tslib-calibrate` (available in `meta-rpi`), and run `ts_calibrate`, and follow the onscreen instructions. The calibration file is saved as `/etc/pointercal`. Recalibrate if you change rotation.

To facilitate the copy of the framebuffers, setup a custom hdmi display for the GPU with the same dimensions as the TFT in `config.txt`.
```bash
hdmi_force_hotplug=1
hdmi_cvt=320 480 60
hdmi_group=2
hdmi_mode=87
```
Install `raspi2fb` and enable it as a startup service:
```bash
root@rpi3:~# cd /etc/rc5.d
root@rpi3:/etc/rc5.d# ln -sf ../init.d/raspi2fb S90raspi2fb
```


### Backlight control

To enable pwm on GPIO_18, edit `/boot/config.txt`:
```bash
# enable pwm on GPIO_18 (for kernel 4.19)
dtoverlay=pwm
```
After reboot you should have the PWM kernel module loaded:
```bash
root@raspberrypi3:~# lsmod | grep pwm
pwm_bcm2835            16384  0
```
If the module is not loaded, you need to add the `overlays/pwm.dtbo` to SD card:
```bash
cp build/tmp/work/raspberrypi3-poky-linux-gnueabi/bcm2835-bootfiles/20191107-r3/firmware-d737daa826adaab8679f83018c3cdc8783d49b59/boot/overlays/pwm.dtbo /media/<your_user>/raspberrypi/overlays/
```

You can now use the following script in order to control the display brightness:
```bash
#!/bin/sh
# disable the GPIO backlight control
echo 0 > /sys/class/backlight/soc\:backlight/brightness
echo 0 > /sys/class/pwm/pwmchip0/export
echo 1000000 > /sys/class/pwm/pwmchip0/pwm0/period
# 30% brightness
echo 300000 > /sys/class/pwm/pwmchip0/pwm0/duty_cycle
echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable
```

### MPU6050 IIO

Make sure you have `build/tmp/work/raspberrypi3-poky-linux-gnueabi/bcm2835-bootfiles/20191107-r3/firmware-d737daa826adaab8679f83018c3cdc8783d49b59/boot/overlays/mpu6050.dtbo` added to the SD card FAT partition overlays directory.

Need to add to config.txt:
```
dtparam=i2c1=on
dtparam=i2c_arm=on
dtoverlay=mpu6050
```

After boot:
```bash
modprobe i2c-dev
```
Note that the interrupt on the MPU6050 must be connected to GPIO pin 4 by default (this can be changed with a dtparam parameter). See README file in overlays directory for more details.

### Configuring and building custom kernel

This starts the linux menuconfig:
* `bitbake linux-yocto -c kernel_configme -f` this ensures that a `.config` file exists (run only if needed, triggers a new kernel rebuild)
* `bitbake -c menuconfig linux-raspberrypi` do your changes
* `bitbake linux-yocto -c diffconfig` creates a `fragment.cfg` somwhere in `build/` directory (a diff that contains the changes done in previous step)

Kernel build path:
`build/tmp/work/raspberrypi3-poky-linux-gnueabi/linux-raspberrypi/1_4.19.81+gitAUTOINC+eef78b714c-r0/linux-raspberrypi3-standard-build`


### Resources:
meta/conf/bitbake.conf -> useful variables (like sysconfdir, bindir)
 <a href="https://jumpnowtek.com/">https://jumpnowtek.com/</a>. Great site for embedded system development.
