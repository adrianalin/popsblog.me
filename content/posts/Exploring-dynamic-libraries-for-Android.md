---
title: "Exploring Dynamic Libraries for Android"
date: 2019-05-22T18:15:02+03:00
draft: false
tags: [ "android" ]
---

In this post I am trying to show what symbols are exported when building a simple C++ library (with Qt) for Android. In the first part i will create the library, and in the second part I will use `nm` in order to get the symbols exported by the library.
Creating the shared library

**Prerequisites**: Qt5.12, Android SDK, Android NDK (r19c), setup Qt Creator for Android development.

Create a new C++ library project in QtCreator: File -> New File or Project -> Library -> C++ library, and you get a template to get you started. I named my library test_lib, here is my `.pro` file:

```bash
QT       -= gui


QT       -= gui

TARGET = test_lib
TEMPLATE = lib

DEFINES += TEST_LIB_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        test_lib.cpp

HEADERS += \
        test_lib.h \
        test_lib_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
```

Here is my `test_lib.h` file:

```C++
#ifndef TEST_LIB_H
#define TEST_LIB_H

#include "test_lib_global.h"
class TEST_LIBSHARED_EXPORT Test_lib
{
public:
    Test_lib();
    void publicMethod();

    static int staticCounter;

private:
    int m_counter=0;
};

#endif // TEST_LIB_H
```

and `test_lib_global.h`:

```C++
#ifndef TEST_LIB_GLOBAL_H
#define TEST_LIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(TEST_LIB_LIBRARY)
#  define TEST_LIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define TEST_LIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // TEST_LIB_GLOBAL_H
```

`test_lib.cpp` :

```C++
#include "test_lib.h"
#include <QDebug>

int Test_lib::staticCounter = 0;

Test_lib::Test_lib()
{
}

void Test_lib::publicMethod()
{
    qDebug() << "This is test 1 printing";
}
```

After building this project you should find a `libtest_lib.so` in your build folder (`build-test_lib-Android_for_armeabi_v7a_Clang_Qt_5_12_3_for_Android_ARMv7-Debug`)

### Get the symbols from the shared library

Use `nm` tool to get the information being used in an object file or executable. Since this is an android library, I use `nm` from ndk-bundle: `/home/adrian/Android/sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/x86_64-linux-android/bin/nm`

`nm` parameters (there are more):

* `g`, `--extern-only` Display only external symbols
* `C`, `--demangle` Decode low-level symbol names into user-level names. Besides removing any initial underscore prepended by the system, this makes C ++ function names readable.
* `A`, `--print-file-name` Print name of the input file before every symbol

To get the output, go to your build directory, and run: `~/Android/sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/x86_64-linux-android/bin/nm -gCA libtest_lib.so`

Here is the output I got:

```bash
libtest_lib.so:         U abort
libtest_lib.so:         U __aeabi_memclr8
libtest_lib.so:         U __aeabi_memcpy
libtest_lib.so:000050ac A __bss_start
libtest_lib.so:         U __cxa_atexit
libtest_lib.so:         U __cxa_begin_catch
libtest_lib.so:         U __cxa_finalize
libtest_lib.so:         U dladdr
libtest_lib.so:000050ac A _edata
libtest_lib.so:000050b1 A _end
libtest_lib.so:         U fflush
libtest_lib.so:         U fprintf
libtest_lib.so:         U __gnu_Unwind_Find_exidx
libtest_lib.so:         U __gxx_personality_v0
libtest_lib.so:         U __sF
libtest_lib.so:         U snprintf
libtest_lib.so:         U __stack_chk_fail
libtest_lib.so:         U __stack_chk_guard
libtest_lib.so:         U strlen
libtest_lib.so:         U QArrayData::deallocate(QArrayData*, unsigned int, unsigned int)
libtest_lib.so:000012c8 W int QAtomicOps<int>::load<int>(std::__ndk1::atomic<int> const&)
libtest_lib.so:00001350 W bool QAtomicOps<int>::deref<int>(std::__ndk1::atomic<int>&)
libtest_lib.so:         U QTextStream::operator<<(char)
libtest_lib.so:         U QTextStream::operator<<(QString const&)
libtest_lib.so:00000f38 W QMessageLogger::QMessageLogger(char const*, int, char const*)
libtest_lib.so:00001244 W QTypedArrayData<unsigned short>::deallocate(QArrayData*)
libtest_lib.so:00001060 W QMessageLogContext::QMessageLogContext(char const*, int, char const*, char const*)
libtest_lib.so:000012a4 W QBasicAtomicInteger<int>::deref()
libtest_lib.so:00001180 W QDebug::maybeSpace()
libtest_lib.so:         U QDebug::~QDebug()
libtest_lib.so:00000f90 W QDebug::operator<<(char const*)
libtest_lib.so:         U QString::fromUtf8_helper(char const*, int)
libtest_lib.so:000010b0 W QString::fromUtf8(char const*, int)
libtest_lib.so:00001128 W QString::~QString()
libtest_lib.so:00000e70 T Test_lib::publicMethod()
libtest_lib.so:000050ac B Test_lib::staticCounter
libtest_lib.so:00000e54 T Test_lib::Test_lib()
libtest_lib.so:00000e54 T Test_lib::Test_lib()
libtest_lib.so:000011cc W QtPrivate::RefCount::deref()
libtest_lib.so:         U QMessageLogger::debug() const
libtest_lib.so:00001284 W QBasicAtomicInteger<int>::load() const
libtest_lib.so:         U std::terminate()
```

The first column contains the file name in which the symbol was found. If we look for our `Test_lib`, we see it is preceded by a `T` or a `B`.

The public method `Test_lib::publicMethod()` is preceded by `T` which means is in the text (code) section. Code section is typically read only, this is the section of object files that contains executable instructions.

The static int `Test_lib::staticCounter` is preceded by `B`, the symbol is in the uninitialized data section (known as BSS ), contains statically allocated variables.

[To be continued]