---
title: "Quick Start With MicroPython on NodeMCU (ESP12 E)"
date: 2019-08-05T18:55:02+03:00
draft: false
tags: [ "nodemcu", "micropython" ]
---

In this post I do a quick description on how to use Micropython on NodeMCU (ESP-12E). Next post will be about reading air parameters: temperature, humidity, pressure and gas using the BME680 integrated environmental sensor.

### Requirements

After connecting the NodeMCU via USB to your computer, follow these steps in linux terminal (note: change `/dev/ttyUSB0` accordingly). First you also need to download the firmware from <a href="https://micropython.org/download#esp8266">https://micropython.org/download#esp8266</a>.

```bash
$ mkdir -p /~/workspace/upython_test && cd !$ 	# create and change to new directory
$ virtualenv venv 								# always work in a virtual environment
$ source venv/bin/activate
$ pip install esptool							# install esptool.py used to flash firmware on NodeMCU
$ esptool.py --port /dev/ttyUSB0 erase_flash	 # erase flash
$ esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 esp8266-20190529-v1.11.bin # flash the firmware downloaded from MicroPython's official site
$ pip install rshell							# remote shell for MicroPython
$ rshell --port /dev/ttyUSB0					# start serial communication with NodeMCU
> repl											# start python REPL (read eval print loop)
>
MicroPython v1.11-8-g48dcbbe60 on 2019-05-29; ESP module with ESP8266
Type "help()" for more information.
>>>
```


### Interacting with REPL

Now we are ready to write some python commands. Notice that all these commands are run by the MicroPython instance you just installed on NodeMCU:
```bash
>>> import esp
>>> esp.check_fw() 				# should end with True
>>> import webrepl_setup 		# command to configure Web REPL
>>> import os 					# list filesystem files
>>> os.listdir()
```

If your devices has 1Mbyte or more of storage then it will be set up (upon first boot) to contain a filesystem. This filesystem uses the FAT format and is stored in the flash after the MicroPython firmware. There are two files that are treated specially by the ESP8266 when it starts up: `boot.py` and `main.py`. The `boot.py` script is executed first (if it exists) and then once it completes, the `main.py` script is executed.


### Checking network

```python
>>> import network 							# check network status
>>> sta_if = network.WLAN(network.STA_IF) 	# station interface
>>> ap_if = network.WLAN(network.AP_IF) 	# access point interface
>>> sta_if.active() 						# by default this is False
>>> ap_if.active() 							# by default this is True
```

Here is a function  that you can add to `boot.py`, that disables NodeMCU WiFi access point and connects to your router. Don't forget to fill in your WiFi credentials.
```bash
>>> def do_connect():
>>>     import network
>>>     sta_if = network.WLAN(network.STA_IF)
>>>     if not sta_if.isconnected():
>>>         print('connecting to network...')
>>>         sta_if.active(True)
>>>         sta_if.connect('<SSID>', '<Password>')
>>>         while not sta_if.isconnected():
>>>             pass
>>>     print('network config:', sta_if.ifconfig())
```


### Blink onboard LEDs:

```bash
>>> import machine
>>> led = machine.Pin(2, machine.Pin.OUT)
>>> led.off()
>>> led.on()
>>> led2 = machine.Pin(16, machine.Pin.OUT)
>>> led2.off()
>>> led2.on()
```


### Explore `rshell` functionalities

Remote Micro Python Shell  is a simple shell which runs on the host and uses MicroPython's raw-REPL to send python snippets to the pyboard in order to get filesystem information, and to copy files to and from MicroPython's filesystem.
```bash
> help

Documented commands (type help <topic>):
========================================
args    cat  connect  echo  exit      filetype  ls     repl  rsync
boards  cd   cp       edit  filesize  help      mkdir  rm    shell

Use Control-D (or the exit command) to exit rshell.
```
This is an interesting command, because it can acces both files on your computer and on dev board. If yo do `> ls /pyboard` will display files on the NodeMCU. 
While in working directory on host, use `> rsync . /pyboard` to get your current directory (all files!) uploaded to NodeMCU (or you can use `cp`). This is the output I get after uploading my project (`README.md` should not be uploaded, this happend when you use `rsync`):
```bash
> ls /pyboard/
si4703/        boot.py        i2c.py         main.py        webrepl_cfg.py README.md
```


### Getting info about memory

```bash
>>> import micropython
>>> micropython.mem_info()
stack: 2112 out of 8192
GC: total: 37952, used: 10832, free: 27120
 No. of 1-blocks: 57, 2-blocks: 13, max blk sz: 264, max free sz: 237
```

If the project you are trying to run on MicroPython is too big, you may get:
```bash
MemoryError: memory allocation failed
```
this may be caused by heap fragmentation. To solve it try `mpy-cross` which runs under any Unix-like system and compiles `.py` scripts into `.mpy` files. Invoke it as:
```bash
$ ./mpy-cross foo.py
```
This will create a file `foo.mpy` which can then be copied to a place accessible by the target MicroPython runtime (eg onto a pyboard's filesystem), and then imported like any other Python module using `import foo`. You also need to delete the `foo.py` file from pyboard because it has precedence at import time.


### What next

Next post will be about using python to read air parameters from BME680 I2C environmental sensor, and maybe send them to a flask web app using websockets.


### Resources

<a href="https://docs.micropython.org/en/latest/esp8266/tutorial/intro.html">https://docs.micropython.org/en/latest/esp8266/tutorial/intro.html</a>

<a href="https://github.com/micropython/micropython/tree/master/mpy-cross">https://github.com/micropython/micropython/tree/master/mpy-cross</a>

<a href="https://github.com/micropython/micropython/issues/2530">https://github.com/micropython/micropython/issues/2530</a>

<a href="https://github.com/dhylands/rshell">https://github.com/dhylands/rshell</a>