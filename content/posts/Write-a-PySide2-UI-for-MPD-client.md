---
title: "Write a PySide2 UI for MPD Client"
date: 2019-07-23T20:51:59+03:00
draft: false
tags: [ "python", "qt" ]
---

This post describes how to write a simple UI for the [MPC](https://adrianalin.gitlab.io/popsblog.me/posts/a-look-at-the-mpd-component-from-homeassistant/) using PySide2 (Python binding for Qt). This does not aim to be a full feature client, there is room for improvement.

![screenshot](https://raw.githubusercontent.com/adrianalin/home-assistant_series/master/python_mpd/screenshot.png)

### Requirements

* `PySide2` it's the Python binding for Qt, install in virtual environment.
* `QtCreator` or `QtDesigner` used for designing the UI layout, add buttons, labels, etc.


### Basics of Qt applications

The core class used when developing Qt applications is `QApplication`. It manages application's control flow and main settings. `QApplication's` responsabilities include:

* initializes the application with user's desktop settings as `palette()`, `font()`, `doubleClickInterval()`

* performs event handling: receives events from te underlying window system and dispatches them to the relevant widgets

* parses command line arguments

* defines application's look and feel which is encapsulated in a `QStyle`object

* specifies how the application is to allocate colors

* provides localization of strings that are visible to the user via `translate()`

* provides some magical objects like the `desktop()` and the `clipboard()`

* knows about the application's windows. You can ask which widget is at a certain position using `widgetAt()`

* manages the application's mouse cursor handling

Event loops are used by all GUI applications. The event loop is kind of an infinite loop that waits for the occurrence of events (user clicking a button, selecting an item from a combobox, pressing a key on the keyboard, and so on).

When a signal is emitted, the slots connected to it are usually executed immediately, just like a normal function call. When this happens, the signals and slots mechanism is totally independent of any GUI event loop. Execution of the code following the `emit` statement will occur once all slots have returned. The situation is slightly different when using queued connections; in such a case, the code following the `emit` keyword will continue immediately, and the slots will be executed later.


### Creating the UI

From QtCreator select New File or Project -> Qt -> Qt Designer Form -> Main Window, name `mainwindow.ui`, and choose to save in project dir, next to the mpd client script from this post [A look at the MPD component from homeassistant.](https://adrianalin.gitlab.io/popsblog.me/posts/a-look-at-the-mpd-component-from-homeassistant)

First add two grid layouts to the main widget. On first grid layout add `playlist_list_widget` and `songs_list_widget`. On the second grid layout add `play_pause_btn`, `volume_dial`, and `now_playing_label`. See the .ui file in github repo.

After designing the ui (or any modifications to .ui file), activate virtual environment and run `pyside2-uic mainwindow.ui > ui_mainwindow.py` to generate the `ui_mainwindow.py`. If you open `ui_mainwindow.py` you see all the UI components we just added in designer, translated to Python.

After all widgets are in place, create the `main.py` file where `QApplication` and `MainWindow` are created. We need to call `app.exec()` to start event handling. The main event loop receives events from the window system and dispatches these to the application widgets. Generally, no user interaction can take place before calling `exec()`.

```python
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = mainwindow.MainWindow()
    window.show()
    sys.exit(app.exec_())
```


The `MainWindow` class inherits from `QMainWindow`, here we do the connections from UI widgets to MPD actions. In constructor instantiate the `Ui_MainWindow` class generated from the `.ui` file and call `setupUi()` which sets up the user interface for the specified widget. Also instantiate the MPD client.

When using Qt framework, we heavily use signals and slots which are used for communication between objects. A signal is emitted when a particular event occurs like pressing `play_pause_btn` or timeout event for a timer. A slot is a function that is called in response to a particular signal. Next populate widgets with data like playlists available on MPD, songs in playlists, MPD status, now playing song title. Also do all widgets' signals slots connections here in constructor only once.

```python
class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.MPD = setup_platform()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.timer = QTimer(self)

        self.update()
        self.ui.playlist_widget.addItems(self.MPD.source_list)

        self.ui.playlist_widget.currentItemChanged.connect(self.select_playlist)
        self.ui.songs_widget.currentItemChanged.connect(self.select_song)
        self.ui.volume_dial.valueChanged.connect(self.volume_changed)
        self.ui.play_pause_btn.pressed.connect(self.play_pause)
        self.timer.timeout.connect(self.update)
        self.timer.start(10000)

    def volume_changed(self, vol):
        self.MPD.set_volume_level(vol / 100)
...
```

Take for example `self.ui.volume_dial.valueChanged.connect(self.volume_changed)` here we connect the signal `valueChanged` of `volume_dial` widget from the ui class to the `self.volume_changed` slot (method), and the new value from `volume_dial` widget is passed as a parameter.

I won't go in details, the rest of signal slots connections are simillar, if you want to learn more about signals slots and widgets I recommend going to C++ documentation for Qt because is more complete and easier to understand.

Before trying out the application you need to fill in your MPD address in `MPD.py` file. Here is the complete github repo https://github.com/adrianalin/python_mpd_client.git.